import Foundation
import RxSwift

let FLVL_OPENLIB_MODE = 1
let ACTIVE_OPENLIB_MODE = FLVL_OPENLIB_MODE
let FL_LIC_FILE = "lic/rdnidlib.dlt"

/// For [CardStatusEvent]
var mbConnectReader:Bool?=false
/// For [CardStatusEvent]
var mbDuringReadCard:Bool?=false
/// For [PercentEvent]
var mPercent: Int64?

class CardReaderSwiftAPIImpl : NSObject, CardReaderSwiftAPI {
    
    fileprivate var mNiOS : NiOS!
    
    let cardStateIsEnable = BehaviorSubject<Bool>(value: false)
    let hardwareStateIsEnable = BehaviorSubject<Bool>(value: false)
    let licenseUpdatedPleaseRestart: Int64 = -19

    
    private var mReaderFtInterface : ReaderInterface?
    
    var mReaderLists: NSMutableArray = []
    
    ///for object
    var mNotifyId: String = ""
    var mMessageType: String = ""
    var mCaller: String = ""
    var mArgData: NSObject?
    
    func initializeCardReader() throws -> InitializeResponse {
        var resResult:String = ""
        var resError:Int64 = 0
        do {
            mNiOS = NiOS();
//                        mReaderFtInterface = ReaderInterface();
//                        mReaderFtInterface?.setDelegate(self)
            //            NotificationCenter.default.addObserver(self, selector: #selector(CardReaderService.OnRDNID_NotifyMessage(_:)), name: NSNotification.Name(rawValue: NiOS_NotifyMessage), object: nil)
            //
            NotificationCenter.default.addObserver(self, selector: #selector(updateBLEItemView(_:)), name: NSNotification.Name(rawValue: NiOS_NotifyMessage), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(updateProgressView(_:)), name: NSNotification.Name(rawValue: NiOS_NotifyMessage), object: nil)
            
            hardwareStateIsEnable.onNext(false)
            cardStateIsEnable.onNext(false)
            
            resResult = "Initialized";
        } catch let error { //Ignore warning s
            
            return InitializeResponse(debugLog: error, errorCode: -1)
        }
        return InitializeResponse(result: resResult);
    }
    
    func getReaders() throws -> GetReadersResponse {
        var resError: Int64;
        var libpath="";
        var openModeLib : Int;
        openModeLib = getLICPath(path: &libpath);
        
        var LICpath : NSMutableString
        LICpath = libpath.mutableCopy() as! NSMutableString;
        var licCheck : Int32?;
        licCheck = mNiOS?.openLibNi(LICpath)
        
        if licCheck != 0 {
            resError = Int64(licCheck!)
            
            if (licCheck == NI_LICENSE_FILE_ERROR) {
                licCheck = mNiOS?.updateLicenseFileNi()
                if licCheck != 0 {
                    resError = Int64(licCheck!)
                }
                else {
                    
resError = licenseUpdatedPleaseRestart
                }
            }
            return GetReadersResponse(errorCode: resError);
        }
        let scanReaders: Int64 = Int64(scanBleReaderThread())
        if scanReaders < 0 {
            
            resError = scanReaders
            
            return GetReadersResponse(errorCode: resError);
        }
        
        return GetReadersResponse(notifyId: mNotifyId, messageType: mMessageType, caller: mCaller,perc: mPercent,argData: mArgData);
    }
    
    func connect(request: ConnectRequest) throws -> ConnectResponse {
        var libPath:String = ""
        var openModeLib : Int;
        openModeLib = getLICPath(path: &libPath)
        
        var res : Int32?;
        let readerName:NSMutableString = NSMutableString(string: request.readerName!)

        res = mNiOS.selectReaderNi(readerName)
        
        let readerInfo = NSMutableString.init();
        if res != 0 {
            var error: Int64;
            var errorLog: String;
            var ntype = getReaderInfo( nsData: readerInfo );
            error = Int64(res!);
            
//            if res == NI_INVALID_LICENSE {
//                mNiOS.closeLibNi();
//                return ConnectResponse(errorCode: error)
//            }
//            
//            if openModeLib == FLVL_OPENLIB_MODE {
//                res = mNiOS?.updateLicenseFileNi()
//                if res != 0 {
//                    errorLog = String(format: "Reader Name: %@\nReader Info: %@\nfunction updateLicenseFileNi: (error code %d)",
//                                      request.readerName!, readerInfo, error)
//                    return ConnectResponse(debugLog: errorLog, errorCode: error)
//                } else {
//                    error = licenseUpdatedPleaseRestart
//                    errorLog = String(format: "Reader Name: %@Reader Info: %@", request.readerName!, readerInfo);
//                    return ConnectResponse(debugLog: errorLog, errorCode: error)
//                }
//            }
//            
//            mNiOS?.closeLibNi()
            errorLog = String(format:"Unknown Error, Reader Info: %@\nError Code %d", readerInfo, error);
            return ConnectResponse(result: false,debugLog:errorLog , errorCode: error)
        } else {
            var msg: String = String(format: "%@  %@" ,request.readerName!,readerInfo);
            mbConnectReader = true;
            return ConnectResponse(result: true, debugLog: msg)
        }
    }

    ///Must work alongside readCardImage
    func readCardTextInfo(request: GetCardInfoRequest) throws -> GetCardInfoResponse {
        var res: Int32?
        
        if mbDuringReadCard == true {
            return GetCardInfoResponse(debugLog: "Card still reading", errorCode: -1);
        }
        
        mbDuringReadCard = true;
        /// Connect to card
        res = mNiOS?.connectCardNi()
        var errorCode = Int64(res!)
        if res != 0 {
            return GetCardInfoResponse(debugLog: "function connectCardNi Error", errorCode: errorCode)
        }
        /// Get data
        let nsData: NSMutableString = NSMutableString()
        res = mNiOS?.getNIDTextNi(nsData)
        errorCode = Int64(res!)
        if res != 0 {
            mbDuringReadCard = false;
            res = mNiOS?.disconnectCardNi()
            return GetCardInfoResponse(debugLog: "function getNIDTextNi Error", errorCode: errorCode)
        }
        
        return GetCardInfoResponse(notifyId: mNotifyId, messageType: mMessageType, caller: mCaller, perc: mPercent, argData: mArgData)
    }
    
    /// Must work alongside readCardTextInfo
    func readCardImage(request: GetCardInfoRequest) -> GetCardInfoResponse {
        var res: Int32?
        let dataPhoto = NSMutableData.init();
        res = mNiOS?.getNIDPhotoNi(dataPhoto)
        var errorCode = Int64(res!)
        if res != 0 {
            res = mNiOS?.disconnectCardNi()
            return GetCardInfoResponse(debugLog: "function getNIDPhotoNi error", errorCode: errorCode)
        }
        res = mNiOS?.disconnectCardNi()
        errorCode = Int64(res!)
        if res != 0 {
            mbDuringReadCard = false;
            return GetCardInfoResponse(debugLog: "function disconnectCardNi error", errorCode: errorCode)
            
        }
        mbDuringReadCard = false;
        return GetCardInfoResponse(notifyId: mNotifyId, messageType: mMessageType, caller: mCaller, perc: mPercent, argData: mArgData)
    }
    
    func disconnect(request: DisconnectRequest) -> DisconnectResponse {
        var res: Int32?;
        
        if mbConnectReader == true && mbDuringReadCard == false {
            res = mNiOS?.deselectReaderNi()
            var errorCode = Int64(res!)
            if res != 0 {
                
                return DisconnectResponse(debugLog: "function deselectReaderNi Error", errorCode: errorCode)
            }
            
            res = mNiOS?.closeLibNi()
            
            if res != 0 {
                return DisconnectResponse(debugLog: "function closeLibNi Error", errorCode: errorCode)
            }
            mbConnectReader = false;
            return DisconnectResponse(result: true)
        } else {
            return DisconnectResponse(result: false)
        }
    }
    
    private func scanBleReaderThread()  -> Int {
        var detectRenderListBLE:NSMutableArray
        
        detectRenderListBLE = NSMutableArray()
        var defaultMaximumDetectedDevice: Int32 = 5;
        
        let resBLE: Int = Int(exactly:mNiOS.scanReaderListBleNi(detectRenderListBLE, defaultMaximumDetectedDevice))!
        
        return resBLE
    }
    
    private func getLICPath(path:inout String ) -> Int {
        var type: Int = -1;
        
        type = ACTIVE_OPENLIB_MODE
        
        switch type
        {
        case FLVL_OPENLIB_MODE:
            var libpath :[ String];
            libpath = NSSearchPathForDirectoriesInDomains( FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true);
            
            path = libpath[0]+"/"+FL_LIC_FILE;
            break;
        default:
            break
        }
        return type;
    }
    
    private func getReaderInfo(nsData: NSMutableString) -> Int32 {
        let res : Int32 = (mNiOS?.getReaderInfoNi(nsData))!
        if res != 0 {
            nsData.append("Reading...")
        }
        return res;
    }
    
    @objc private func updateBLEItemView(_ notification: Notification) {
        if let userInfo  = notification.userInfo, let info = NotificationUserInfo(userInfo: userInfo) {
            
            if info.Caller.compare("scanReaderListBleNi") == ComparisonResult.orderedSame {
                let readerName:String = info.arg as! String
                mNotifyId = info.NotifyID
                mMessageType = info.MessegeType
                mCaller = info.Caller
                mPercent = info.perc ?? 0
                mArgData = info.arg
                mReaderLists.add(readerName)
            }
        }
    }
        
    @objc private func updateProgressView(_ notification: Notification) {
        if let userInfo  = notification.userInfo, let info = NotificationUserInfo(userInfo: userInfo) {
            if info.Caller.compare("getNIDTextNi") == ComparisonResult.orderedSame ||
                info.Caller.compare("getATextNi") == ComparisonResult.orderedSame {
                if info.perc! >= 100 {
                    mNotifyId = info.NotifyID
                    mMessageType = info.MessegeType
                    mCaller = info.Caller
                    mPercent = info.perc ?? 0
                    mArgData = info.arg
                }
            }
            
            if info.Caller.compare("getNIDPhotoNi") == ComparisonResult.orderedSame {
                mPercent = info.perc;
                if info.perc! >= 100 {
                    mNotifyId = info.NotifyID
                    mMessageType = info.MessegeType
                    mCaller = info.Caller
                    mPercent = info.perc ?? 0
                    mArgData = info.arg
                }
            }
        }
    }
    
    struct NotificationUserInfo {
        var NotifyID: String
        var MessegeType: String
        var Caller: String
        var perc: Int64?
        var arg: NSObject?
        
        init?(userInfo: [AnyHashable: Any]) {
            guard let notifyId = userInfo["NotifyId"] as? String,
                  let messegeType = userInfo["MessageType"] as? String,
                  let caller = userInfo["Caller"] as? String else {
                return nil
            }
            self.NotifyID = notifyId
            self.MessegeType = messegeType
            self.Caller = caller
            self.perc = userInfo["Percent"] as? Int64
            self.arg = userInfo["ArgData"] as? NSObject
        }
    }
    
}


//extension CardReaderSwiftAPIImpl: ReaderInterfaceDelegate {
//    func findPeripheralReader(_ readerName: String!) {
//        
//    }
//    
//    func readerInterfaceDidChange(_ attached: Bool, bluetoothID: String!) {
//        
//    }
//    
//    func didGetBattery(_ battery: Int) {
//        
//    }
//    
//    
//    @objc func ShowHardwareStateON(_ parm: AnyObject) {
//        hardwareStateIsEnable.onNext(true)
//    }
//    
//    @objc func ShowHardwareStateOFF(_ parm: AnyObject) {
//        hardwareStateIsEnable.onNext(false)
//    }
//    
//    @objc func ShowCardStateON(_ parm: AnyObject) {
//        cardStateIsEnable.onNext(true)
//    }
//    
//    @objc func ShowCardStateOFF(_ parm: AnyObject) {
//        cardStateIsEnable.onNext(false)
//    }
//    
//    
//    /**
//     reader is Attach or DisAttach
//     - parameter attached: void
//     */
//    func readerInterfaceDidChange(_ attached: Bool) {
////        //"Experiment only, don't use for real product"
//        if attached
//        {
//            DispatchQueue.main.async(execute: { () -> Void in
//                self.performSelector(onMainThread: #selector(CardReaderSwiftAPIImpl.ShowHardwareStateON(_:)), with: Int(truncating: true), waitUntilDone: true)
//            })
//        } else{
//            DispatchQueue.main.async(execute: { () -> Void in
//                self.performSelector(onMainThread: #selector(CardReaderSwiftAPIImpl.ShowHardwareStateOFF(_:)), with: Int(truncating: true), waitUntilDone: true)
//            })
//        }
//    }
//    
//    /**
//     card Attach or DisAttach
//     
//     - parameter attached: void
//     */
//    func cardInterfaceDidDetach(_ attached: Bool) {
////        //"Experiment only, don't use for real product"
//        if attached
//        {
//            self.performSelector(onMainThread: #selector(CardReaderSwiftAPIImpl.ShowCardStateON(_:)), with: Int(truncating: true), waitUntilDone: true)
//            
//        } else{
//            self.performSelector(onMainThread: #selector(CardReaderSwiftAPIImpl.ShowCardStateOFF(_:)), with: Int(truncating: true), waitUntilDone: true)
//        }
//        
//    }
//}
