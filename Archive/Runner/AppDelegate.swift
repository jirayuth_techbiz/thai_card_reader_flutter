import Flutter
import UIKit

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      let cardReaderServiceAPI = CardReaderSwiftAPIImpl()
      let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
      /// Set ReaderType
      NiOS.setReaderTypeNi( READERTYPE_BLE )
      ///Setup Method Controller
      CardReaderSwiftAPISetup.setUp(binaryMessenger: controller.binaryMessenger, api: cardReaderServiceAPI)
      
      ///Setup Event Controller, Pigeon doesn't suppoer Event Channel yet.
      let cardStatusHandlerEvent = FlutterEventChannel(name: "cardStatusHandlerEvent", binaryMessenger: controller.binaryMessenger)
      cardStatusHandlerEvent.setStreamHandler(CardStatusEvent())
      
      let cardCurrentlyReadHandlerEvent = FlutterEventChannel(name: "cardCurrentlyReadHandlerEvent", binaryMessenger: controller.binaryMessenger)
      cardCurrentlyReadHandlerEvent.setStreamHandler(CardReadingEvent())
      
      let percentHandlerEvent = FlutterEventChannel(name: "percentHandlerEvent", binaryMessenger: controller.binaryMessenger)
      percentHandlerEvent.setStreamHandler(PercentEvent())
      
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}

