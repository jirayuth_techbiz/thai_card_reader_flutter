//
//  PercentEvent.swift
//  Runner
//
//  Created by Tcb Devbuild on 1/7/2567 BE.
//

import Foundation
import RxSwift
import Flutter

class PercentEvent: NSObject, FlutterStreamHandler {
    private var eventSink: FlutterEventSink?
    private let disposeBag = DisposeBag()
    let percentProgress = BehaviorSubject<Int64>(value: 0)
    
    override init() {
        super.init()
        percentProgress.onNext(mPercent ?? 0)
    }
    
    func onListen(withArguments arguments: Any?, eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink

        percentProgress.distinctUntilChanged().subscribe(onNext: {[weak self] perc in 
            let percentProgress = perc
            self?.eventSink?(percentProgress)}).disposed(by: disposeBag);
        
        let initialVal = percentProgress.value
        eventSink(initialVal)
        
        return nil;
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil;
        return nil;
    }
    
    
}
