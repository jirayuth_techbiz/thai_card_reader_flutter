// Autogenerated from Pigeon (v20.0.2), do not edit directly.
// See also: https://pub.dev/packages/pigeon

import Foundation

#if os(iOS)
  import Flutter
#elseif os(macOS)
  import FlutterMacOS
#else
  #error("Unsupported platform.")
#endif

/// Error class for passing custom error details to Dart side.
final class PigeonError: Error {
  let code: String
  let message: String?
  let details: Any?

  init(code: String, message: String?, details: Any?) {
    self.code = code
    self.message = message
    self.details = details
  }

  var localizedDescription: String {
    return
      "PigeonError(code: \(code), message: \(message ?? "<nil>"), details: \(details ?? "<nil>")"
      }
}

private func wrapResult(_ result: Any?) -> [Any?] {
  return [result]
}

private func wrapError(_ error: Any) -> [Any?] {
  if let pigeonError = error as? PigeonError {
    return [
      pigeonError.code,
      pigeonError.message,
      pigeonError.details,
    ]
  }
  if let flutterError = error as? FlutterError {
    return [
      flutterError.code,
      flutterError.message,
      flutterError.details,
    ]
  }
  return [
    "\(error)",
    "\(type(of: error))",
    "Stacktrace: \(Thread.callStackSymbols)",
  ]
}

private func isNullish(_ value: Any?) -> Bool {
  return value is NSNull || value == nil
}

private func nilOrValue<T>(_ value: Any?) -> T? {
  if value is NSNull { return nil }
  return value as! T?
}

/// https://blog.codemagic.io/working-with-native-elements/
///
/// Generated class from Pigeon that represents data sent in messages.
struct InitializeResponse {
  var result: String? = nil
  var debugLog: Any? = nil
  var errorCode: Int64? = nil

  // swift-format-ignore: AlwaysUseLowerCamelCase
  static func fromList(_ __pigeon_list: [Any?]) -> InitializeResponse? {
    let result: String? = nilOrValue(__pigeon_list[0])
    let debugLog: Any? = __pigeon_list[1]
    let errorCode: Int64? = isNullish(__pigeon_list[2]) ? nil : (__pigeon_list[2] is Int64? ? __pigeon_list[2] as! Int64? : Int64(__pigeon_list[2] as! Int32))

    return InitializeResponse(
      result: result,
      debugLog: debugLog,
      errorCode: errorCode
    )
  }
  func toList() -> [Any?] {
    return [
      result,
      debugLog,
      errorCode,
    ]
  }
}

/// Generated class from Pigeon that represents data sent in messages.
struct GetReadersResponse {
  var notifyId: String? = nil
  var messageType: String? = nil
  var caller: String? = nil
  var perc: Int64? = nil
  var argData: Any? = nil
  var debugLog: Any? = nil
  var errorCode: Int64? = nil

  // swift-format-ignore: AlwaysUseLowerCamelCase
  static func fromList(_ __pigeon_list: [Any?]) -> GetReadersResponse? {
    let notifyId: String? = nilOrValue(__pigeon_list[0])
    let messageType: String? = nilOrValue(__pigeon_list[1])
    let caller: String? = nilOrValue(__pigeon_list[2])
    let perc: Int64? = isNullish(__pigeon_list[3]) ? nil : (__pigeon_list[3] is Int64? ? __pigeon_list[3] as! Int64? : Int64(__pigeon_list[3] as! Int32))
    let argData: Any? = __pigeon_list[4]
    let debugLog: Any? = __pigeon_list[5]
    let errorCode: Int64? = isNullish(__pigeon_list[6]) ? nil : (__pigeon_list[6] is Int64? ? __pigeon_list[6] as! Int64? : Int64(__pigeon_list[6] as! Int32))

    return GetReadersResponse(
      notifyId: notifyId,
      messageType: messageType,
      caller: caller,
      perc: perc,
      argData: argData,
      debugLog: debugLog,
      errorCode: errorCode
    )
  }
  func toList() -> [Any?] {
    return [
      notifyId,
      messageType,
      caller,
      perc,
      argData,
      debugLog,
      errorCode,
    ]
  }
}

/// Generated class from Pigeon that represents data sent in messages.
struct ConnectRequest {
  var readerName: String? = nil

  // swift-format-ignore: AlwaysUseLowerCamelCase
  static func fromList(_ __pigeon_list: [Any?]) -> ConnectRequest? {
    let readerName: String? = nilOrValue(__pigeon_list[0])

    return ConnectRequest(
      readerName: readerName
    )
  }
  func toList() -> [Any?] {
    return [
      readerName
    ]
  }
}

/// Generated class from Pigeon that represents data sent in messages.
struct ConnectResponse {
  var result: Bool? = nil
  var debugLog: Any? = nil
  var errorCode: Int64? = nil

  // swift-format-ignore: AlwaysUseLowerCamelCase
  static func fromList(_ __pigeon_list: [Any?]) -> ConnectResponse? {
    let result: Bool? = nilOrValue(__pigeon_list[0])
    let debugLog: Any? = __pigeon_list[1]
    let errorCode: Int64? = isNullish(__pigeon_list[2]) ? nil : (__pigeon_list[2] is Int64? ? __pigeon_list[2] as! Int64? : Int64(__pigeon_list[2] as! Int32))

    return ConnectResponse(
      result: result,
      debugLog: debugLog,
      errorCode: errorCode
    )
  }
  func toList() -> [Any?] {
    return [
      result,
      debugLog,
      errorCode,
    ]
  }
}

/// Generated class from Pigeon that represents data sent in messages.
struct GetCardInfoRequest {
  var readerName: String? = nil

  // swift-format-ignore: AlwaysUseLowerCamelCase
  static func fromList(_ __pigeon_list: [Any?]) -> GetCardInfoRequest? {
    let readerName: String? = nilOrValue(__pigeon_list[0])

    return GetCardInfoRequest(
      readerName: readerName
    )
  }
  func toList() -> [Any?] {
    return [
      readerName
    ]
  }
}

/// Generated class from Pigeon that represents data sent in messages.
struct GetCardInfoResponse {
  var notifyId: String? = nil
  var messageType: String? = nil
  var caller: String? = nil
  var perc: Int64? = nil
  var argData: Any? = nil
  var debugLog: Any? = nil
  var errorCode: Int64? = nil

  // swift-format-ignore: AlwaysUseLowerCamelCase
  static func fromList(_ __pigeon_list: [Any?]) -> GetCardInfoResponse? {
    let notifyId: String? = nilOrValue(__pigeon_list[0])
    let messageType: String? = nilOrValue(__pigeon_list[1])
    let caller: String? = nilOrValue(__pigeon_list[2])
    let perc: Int64? = isNullish(__pigeon_list[3]) ? nil : (__pigeon_list[3] is Int64? ? __pigeon_list[3] as! Int64? : Int64(__pigeon_list[3] as! Int32))
    let argData: Any? = __pigeon_list[4]
    let debugLog: Any? = __pigeon_list[5]
    let errorCode: Int64? = isNullish(__pigeon_list[6]) ? nil : (__pigeon_list[6] is Int64? ? __pigeon_list[6] as! Int64? : Int64(__pigeon_list[6] as! Int32))

    return GetCardInfoResponse(
      notifyId: notifyId,
      messageType: messageType,
      caller: caller,
      perc: perc,
      argData: argData,
      debugLog: debugLog,
      errorCode: errorCode
    )
  }
  func toList() -> [Any?] {
    return [
      notifyId,
      messageType,
      caller,
      perc,
      argData,
      debugLog,
      errorCode,
    ]
  }
}

/// Generated class from Pigeon that represents data sent in messages.
struct DisconnectRequest {
  var readerName: String? = nil

  // swift-format-ignore: AlwaysUseLowerCamelCase
  static func fromList(_ __pigeon_list: [Any?]) -> DisconnectRequest? {
    let readerName: String? = nilOrValue(__pigeon_list[0])

    return DisconnectRequest(
      readerName: readerName
    )
  }
  func toList() -> [Any?] {
    return [
      readerName
    ]
  }
}

/// Generated class from Pigeon that represents data sent in messages.
struct DisconnectResponse {
  var result: Bool? = nil
  var debugLog: Any? = nil
  var errorCode: Int64? = nil

  // swift-format-ignore: AlwaysUseLowerCamelCase
  static func fromList(_ __pigeon_list: [Any?]) -> DisconnectResponse? {
    let result: Bool? = nilOrValue(__pigeon_list[0])
    let debugLog: Any? = __pigeon_list[1]
    let errorCode: Int64? = isNullish(__pigeon_list[2]) ? nil : (__pigeon_list[2] is Int64? ? __pigeon_list[2] as! Int64? : Int64(__pigeon_list[2] as! Int32))

    return DisconnectResponse(
      result: result,
      debugLog: debugLog,
      errorCode: errorCode
    )
  }
  func toList() -> [Any?] {
    return [
      result,
      debugLog,
      errorCode,
    ]
  }
}
private class AppDelegatePigeonCodecReader: FlutterStandardReader {
  override func readValue(ofType type: UInt8) -> Any? {
    switch type {
    case 129:
      return InitializeResponse.fromList(self.readValue() as! [Any?])
    case 130:
      return GetReadersResponse.fromList(self.readValue() as! [Any?])
    case 131:
      return ConnectRequest.fromList(self.readValue() as! [Any?])
    case 132:
      return ConnectResponse.fromList(self.readValue() as! [Any?])
    case 133:
      return GetCardInfoRequest.fromList(self.readValue() as! [Any?])
    case 134:
      return GetCardInfoResponse.fromList(self.readValue() as! [Any?])
    case 135:
      return DisconnectRequest.fromList(self.readValue() as! [Any?])
    case 136:
      return DisconnectResponse.fromList(self.readValue() as! [Any?])
    default:
      return super.readValue(ofType: type)
    }
  }
}

private class AppDelegatePigeonCodecWriter: FlutterStandardWriter {
  override func writeValue(_ value: Any) {
    if let value = value as? InitializeResponse {
      super.writeByte(129)
      super.writeValue(value.toList())
    } else if let value = value as? GetReadersResponse {
      super.writeByte(130)
      super.writeValue(value.toList())
    } else if let value = value as? ConnectRequest {
      super.writeByte(131)
      super.writeValue(value.toList())
    } else if let value = value as? ConnectResponse {
      super.writeByte(132)
      super.writeValue(value.toList())
    } else if let value = value as? GetCardInfoRequest {
      super.writeByte(133)
      super.writeValue(value.toList())
    } else if let value = value as? GetCardInfoResponse {
      super.writeByte(134)
      super.writeValue(value.toList())
    } else if let value = value as? DisconnectRequest {
      super.writeByte(135)
      super.writeValue(value.toList())
    } else if let value = value as? DisconnectResponse {
      super.writeByte(136)
      super.writeValue(value.toList())
    } else {
      super.writeValue(value)
    }
  }
}

private class AppDelegatePigeonCodecReaderWriter: FlutterStandardReaderWriter {
  override func reader(with data: Data) -> FlutterStandardReader {
    return AppDelegatePigeonCodecReader(data: data)
  }

  override func writer(with data: NSMutableData) -> FlutterStandardWriter {
    return AppDelegatePigeonCodecWriter(data: data)
  }
}

class AppDelegatePigeonCodec: FlutterStandardMessageCodec, @unchecked Sendable {
  static let shared = AppDelegatePigeonCodec(readerWriter: AppDelegatePigeonCodecReaderWriter())
}

/// Generated protocol from Pigeon that represents a handler of messages from Flutter.
protocol CardReaderSwiftAPI {
  ///เริ่มระบบ Card Reader หลังบ้าน
  func initializeCardReader() throws -> InitializeResponse
  ///อ่านข้อมูลเครื่องอ่านบัตรทั้งหมดในละแวก
  func getReaders() throws -> GetReadersResponse
  ///เชื่อมต่อเครื่องอ่านบัตรที่เลือก
  func connect(request: ConnectRequest) throws -> ConnectResponse
  /// ตัดการเชื่อมต่อเครื่องอ่านบัตร
  func disconnect(request: DisconnectRequest) throws -> DisconnectResponse
  ///อ่านข้อมูลบัตรที่เป็น Text-Based
  func readCardTextInfo(request: GetCardInfoRequest) throws -> GetCardInfoResponse
  ///อ่านข้อมูลรูปบัตร รูปบัตรจะอยู่ในรูปแบบ [Base64] ต้อง decode ก่อนใช้งาน
  func readCardImage(request: GetCardInfoRequest) throws -> GetCardInfoResponse
}

/// Generated setup class from Pigeon to handle messages through the `binaryMessenger`.
class CardReaderSwiftAPISetup {
  static var codec: FlutterStandardMessageCodec { AppDelegatePigeonCodec.shared }
  /// Sets up an instance of `CardReaderSwiftAPI` to handle messages through the `binaryMessenger`.
  static func setUp(binaryMessenger: FlutterBinaryMessenger, api: CardReaderSwiftAPI?, messageChannelSuffix: String = "") {
    let channelSuffix = messageChannelSuffix.count > 0 ? ".\(messageChannelSuffix)" : ""
    ///เริ่มระบบ Card Reader หลังบ้าน
    let initializeCardReaderChannel = FlutterBasicMessageChannel(name: "dev.flutter.pigeon.com.techbusiness.icare.cardreader.CardReaderSwiftAPI.initializeCardReader\(channelSuffix)", binaryMessenger: binaryMessenger, codec: codec)
    if let api = api {
      initializeCardReaderChannel.setMessageHandler { _, reply in
        do {
          let result = try api.initializeCardReader()
          reply(wrapResult(result))
        } catch {
          reply(wrapError(error))
        }
      }
    } else {
      initializeCardReaderChannel.setMessageHandler(nil)
    }
    ///อ่านข้อมูลเครื่องอ่านบัตรทั้งหมดในละแวก
    let getReadersChannel = FlutterBasicMessageChannel(name: "dev.flutter.pigeon.com.techbusiness.icare.cardreader.CardReaderSwiftAPI.getReaders\(channelSuffix)", binaryMessenger: binaryMessenger, codec: codec)
    if let api = api {
      getReadersChannel.setMessageHandler { _, reply in
        do {
          let result = try api.getReaders()
          reply(wrapResult(result))
        } catch {
          reply(wrapError(error))
        }
      }
    } else {
      getReadersChannel.setMessageHandler(nil)
    }
    ///เชื่อมต่อเครื่องอ่านบัตรที่เลือก
    let connectChannel = FlutterBasicMessageChannel(name: "dev.flutter.pigeon.com.techbusiness.icare.cardreader.CardReaderSwiftAPI.connect\(channelSuffix)", binaryMessenger: binaryMessenger, codec: codec)
    if let api = api {
      connectChannel.setMessageHandler { message, reply in
        let args = message as! [Any?]
        let requestArg = args[0] as! ConnectRequest
        do {
          let result = try api.connect(request: requestArg)
          reply(wrapResult(result))
        } catch {
          reply(wrapError(error))
        }
      }
    } else {
      connectChannel.setMessageHandler(nil)
    }
    /// ตัดการเชื่อมต่อเครื่องอ่านบัตร
    let disconnectChannel = FlutterBasicMessageChannel(name: "dev.flutter.pigeon.com.techbusiness.icare.cardreader.CardReaderSwiftAPI.disconnect\(channelSuffix)", binaryMessenger: binaryMessenger, codec: codec)
    if let api = api {
      disconnectChannel.setMessageHandler { message, reply in
        let args = message as! [Any?]
        let requestArg = args[0] as! DisconnectRequest
        do {
          let result = try api.disconnect(request: requestArg)
          reply(wrapResult(result))
        } catch {
          reply(wrapError(error))
        }
      }
    } else {
      disconnectChannel.setMessageHandler(nil)
    }
    ///อ่านข้อมูลบัตรที่เป็น Text-Based
    let readCardTextInfoChannel = FlutterBasicMessageChannel(name: "dev.flutter.pigeon.com.techbusiness.icare.cardreader.CardReaderSwiftAPI.readCardTextInfo\(channelSuffix)", binaryMessenger: binaryMessenger, codec: codec)
    if let api = api {
      readCardTextInfoChannel.setMessageHandler { message, reply in
        let args = message as! [Any?]
        let requestArg = args[0] as! GetCardInfoRequest
        do {
          let result = try api.readCardTextInfo(request: requestArg)
          reply(wrapResult(result))
        } catch {
          reply(wrapError(error))
        }
      }
    } else {
      readCardTextInfoChannel.setMessageHandler(nil)
    }
    ///อ่านข้อมูลรูปบัตร รูปบัตรจะอยู่ในรูปแบบ [Base64] ต้อง decode ก่อนใช้งาน
    let readCardImageChannel = FlutterBasicMessageChannel(name: "dev.flutter.pigeon.com.techbusiness.icare.cardreader.CardReaderSwiftAPI.readCardImage\(channelSuffix)", binaryMessenger: binaryMessenger, codec: codec)
    if let api = api {
      readCardImageChannel.setMessageHandler { message, reply in
        let args = message as! [Any?]
        let requestArg = args[0] as! GetCardInfoRequest
        do {
          let result = try api.readCardImage(request: requestArg)
          reply(wrapResult(result))
        } catch {
          reply(wrapError(error))
        }
      }
    } else {
      readCardImageChannel.setMessageHandler(nil)
    }
  }
}
