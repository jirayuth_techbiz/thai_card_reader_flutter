//
//  cardStatusStreamHandler.swift
//  Runner
//
//  Created by Tcb Devbuild on 1/7/2567 BE.
//

import Foundation
import RxSwift
import Flutter

class CardStatusEvent: NSObject, FlutterStreamHandler {
    fileprivate var mIsCardConnect:Bool? = false;
    
    private var eventSink: FlutterEventSink?
    
    private let IsCardConnectStream = BehaviorSubject<Bool?>(value: false)
    
    private let disposeBag = DisposeBag()
    
    override init() {
        super.init()
        IsCardConnectStream.onNext(mbConnectReader)
    }
    
    func onListen(withArguments arguments: Any?, eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink;
        
        IsCardConnectStream.distinctUntilChanged().subscribe(onNext: {[weak self] isConnected in 
            let connectionStatus = isConnected ?? false
            self?.eventSink?(connectionStatus)}).disposed(by:disposeBag)
        
        let initialConnectionStatus = IsCardConnectStream.value
        eventSink(initialConnectionStatus)
        
        return nil;
        
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil;
        return nil;
    }
}

class CardReadingEvent: NSObject, FlutterStreamHandler {
    fileprivate var mIsCardDuringRead:Bool? = false;
    
    private var eventSink: FlutterEventSink?
    
    private let IsCardCurrentlyReadingStream = BehaviorSubject<Bool?>(value: false)
    
    private let disposeBag = DisposeBag()
    
    override init() {
        super.init()
        IsCardCurrentlyReadingStream.onNext(mbDuringReadCard)
    }
    
    func onListen(withArguments arguments: Any?, eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink;
        
        IsCardCurrentlyReadingStream.distinctUntilChanged().subscribe(onNext: {[weak self] isConnected in 
            let IsCardCurrentlyReading = isConnected ?? false
            self?.eventSink?(IsCardCurrentlyReading)}).disposed(by:disposeBag)
        let initialConnectionStatus = IsCardCurrentlyReadingStream.value
        eventSink(initialConnectionStatus)
        
        return nil;
        
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil;
        return nil;
    }
}
