//
//  PercentEvent.swift
//  Runner
//
//  Created by Tcb Devbuild on 1/7/2567 BE.
//

import Foundation
import RxSwift
#if os(iOS)
  import Flutter
#elseif os(macOS)
  import FlutterMacOS
#endif

class PercentEvent: NSObject, FlutterStreamHandler {
    private var eventSink: FlutterEventSink?
    private let disposeBag = DisposeBag()
    let percentProgress = BehaviorSubject<Int64>(value: 0)
    
    override init() {
        super.init()
    }
    
    func onListen(withArguments arguments: Any?, eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink
        
        CardReaderSwiftAPIImpl.shared.percentageProgress.subscribe(onNext: {[weak self] perc in
            DispatchQueue.main.async {
                let percentProgress = perc
                self!.eventSink!(percentProgress)}
        }).disposed(by: disposeBag);
            

        return nil;
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil;
        return nil;
    }
    
    
}
