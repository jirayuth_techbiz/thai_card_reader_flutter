import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get title => 'ICS iCare Card Reader';

  @override
  String get confirmTxt => 'Confirm';

  @override
  String get okTxt => 'Okay';

  @override
  String get returnTxt => 'Return';

  @override
  String get cancelTxt => 'Cancel';

  @override
  String get errorTxt => 'Error';

  @override
  String get retryTxt => 'Retry';

  @override
  String get closeTxt => 'Close';

  @override
  String get refreshTxt => 'รีเฟรช';

  @override
  String get male => 'Male';

  @override
  String get female => 'Female';

  @override
  String get homeTitle => 'Welcome,\nICS iCare Card Reader';

  @override
  String get homeConnect => 'Connect';

  @override
  String get homeErrorNoDevice => '\'No device found, please connect the device and try again\'';

  @override
  String get homeErrorConnect => 'Cannot connect to the device, Refresh and try again';

  @override
  String get cardReaderTitle => 'ID Card Information';

  @override
  String get cardReaderStatus => 'Status : ';

  @override
  String get cardReaderStatusDisconnect => 'Disconnected';

  @override
  String get cardReaderStatusConnect => 'Connected';

  @override
  String get cardReaderReadSmartCardButton => 'Read Smart Card';

  @override
  String get cardReaderDeviceName => 'Device Name : ';

  @override
  String get cardReaderCitizenID => 'Thai National ID';

  @override
  String get cardReaderThPrefix => 'Thai Title';

  @override
  String get cardReaderThFirstName => 'First Name (TH)';

  @override
  String get cardReaderThLastName => 'Last Name (TH)';

  @override
  String get cardReaderEnPrefix => 'English Title';

  @override
  String get cardReaderEnFirstName => 'First Name (EN)';

  @override
  String get cardReaderEnLastName => 'Last Name (EN)';

  @override
  String get cardReaderGender => 'Gender';

  @override
  String get cardReaderBirthDate => 'Birth Date';

  @override
  String get cardReaderAddress => 'Full Address';

  @override
  String get cardReaderSubDistricts => 'Sub District';

  @override
  String get cardReaderDistricts => 'District';

  @override
  String get cardReaderProvinces => 'Province';

  @override
  String get cardReaderOpenICSLink => 'Open ICS';

  @override
  String get successNoErrorTxt => 'Success';

  @override
  String get internalError => 'Internal error, please try again or contact developer.';

  @override
  String get invalidLicense => 'Invalid license, please try again.';

  @override
  String get readerNotFound => 'Reader not found, please make sure your device is on and try again.';

  @override
  String get connectionError => 'Connection to card error, please try again.';

  @override
  String get getPhotoError => 'Error while getting photo from the smartcard, please try again.';

  @override
  String get getPersonalInfoTxtError => 'Error while getting personal information from the smartcard, please try again.';

  @override
  String get invalidCard => 'Invalid card, please make sure you insert correct card.';

  @override
  String get unknownCardVersion => 'Unknown card version, please try again.';

  @override
  String get disconnectionError => 'Error while disconnecting the card reader, please try again.';

  @override
  String get initError => 'Error while initializing the card reader, please try again.';

  @override
  String get readerNotSupportedError => 'Reader not supported, please change your device.';

  @override
  String get licenseFileError => 'Error while reading license file, Your license may be corrupted. please try again.';

  @override
  String get parameterError => 'Parameter error, please try again. If the issue persists, Contact the developer.';

  @override
  String get internetError => 'Internet connection error, please check your internet connection and try again.';

  @override
  String get cardNotFoundError => 'Card not found, please make sure you insert the card correctly.';

  @override
  String get licenseUpdateError => 'Error while updating license, please try again.';

  @override
  String get licenseUpdateSuccess => 'License updated successfully. Please reconnect your device again.';
}
