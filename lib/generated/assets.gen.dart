/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';

class $AssetsBgGen {
  const $AssetsBgGen();

  /// File path: assets/bg/bg.jpeg
  AssetGenImage get bg => const AssetGenImage('assets/bg/bg.jpeg');

  /// List of all assets
  List<AssetGenImage> get values => [bg];
}

class $AssetsDesignGen {
  const $AssetsDesignGen();

  /// File path: assets/design/download.jpeg
  AssetGenImage get download =>
      const AssetGenImage('assets/design/download.jpeg');

  /// List of all assets
  List<AssetGenImage> get values => [download];
}

class $AssetsIconGen {
  const $AssetsIconGen();

  /// File path: assets/icon/152.png
  AssetGenImage get a152 => const AssetGenImage('assets/icon/152.png');

  /// File path: assets/icon/167.png
  AssetGenImage get a167 => const AssetGenImage('assets/icon/167.png');

  /// File path: assets/icon/20.png
  AssetGenImage get a20 => const AssetGenImage('assets/icon/20.png');

  /// File path: assets/icon/29.png
  AssetGenImage get a29 => const AssetGenImage('assets/icon/29.png');

  /// File path: assets/icon/40-1.png
  AssetGenImage get a401 => const AssetGenImage('assets/icon/40-1.png');

  /// File path: assets/icon/40.png
  AssetGenImage get a40 => const AssetGenImage('assets/icon/40.png');

  /// File path: assets/icon/58.png
  AssetGenImage get a58 => const AssetGenImage('assets/icon/58.png');

  /// File path: assets/icon/76.png
  AssetGenImage get a76 => const AssetGenImage('assets/icon/76.png');

  /// File path: assets/icon/80.png
  AssetGenImage get a80 => const AssetGenImage('assets/icon/80.png');

  /// File path: assets/icon/appstore.png
  AssetGenImage get appstore => const AssetGenImage('assets/icon/appstore.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [a152, a167, a20, a29, a401, a40, a58, a76, a80, appstore];
}

class Assets {
  Assets._();

  static const $AssetsBgGen bg = $AssetsBgGen();
  static const $AssetsDesignGen design = $AssetsDesignGen();
  static const $AssetsIconGen icon = $AssetsIconGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName, {this.size = null});

  final String _assetName;

  final Size? size;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
