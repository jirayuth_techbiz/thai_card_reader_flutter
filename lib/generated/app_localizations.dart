import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_en.dart';
import 'app_localizations_th.dart';

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'generated/app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('en'),
    Locale('th')
  ];

  /// ชื่อแอพ
  ///
  /// In th, this message translates to:
  /// **'แอพ iCare Card Reader'**
  String get title;

  /// ปุ่มยืนยัน
  ///
  /// In th, this message translates to:
  /// **'ยืนยัน'**
  String get confirmTxt;

  /// ปุ่มตกลง
  ///
  /// In th, this message translates to:
  /// **'ตกลง'**
  String get okTxt;

  /// ปุ่มย้อนกลับ
  ///
  /// In th, this message translates to:
  /// **'ย้อนกลับ'**
  String get returnTxt;

  /// ปุ่มยกเลิก
  ///
  /// In th, this message translates to:
  /// **'ยกเลิก'**
  String get cancelTxt;

  /// คำว่าผิดพลาด แจ้งเวลาเกิดข้อผิดพลาดใดๆ
  ///
  /// In th, this message translates to:
  /// **'ผิดพลาด'**
  String get errorTxt;

  /// ปุ่มลองอีกครั้ง
  ///
  /// In th, this message translates to:
  /// **'ลองอีกครั้ง'**
  String get retryTxt;

  /// ปุ่มปิดรายการหรือหน้าต่าง
  ///
  /// In th, this message translates to:
  /// **'ปิด'**
  String get closeTxt;

  /// ปุ่มรีเฟรชหน้า
  ///
  /// In th, this message translates to:
  /// **'รีเฟรช'**
  String get refreshTxt;

  /// Male
  ///
  /// In th, this message translates to:
  /// **'ชาย'**
  String get male;

  /// Female
  ///
  /// In th, this message translates to:
  /// **'หญิง'**
  String get female;

  /// Title of the home screen
  ///
  /// In th, this message translates to:
  /// **'ยินดีต้อนรับสู่,\nICS iCare Card Reader'**
  String get homeTitle;

  /// ปุ่มเชื่อมต่อเครื่องสแกนบัตร
  ///
  /// In th, this message translates to:
  /// **'เชื่อมต่อ'**
  String get homeConnect;

  /// แจ้งผู้ใช้ว่าไม่พบเครื่องสแกนบัตร ให้เชื่อมต่อเครื่องสแกนบัตรและลองอีกครั้ง
  ///
  /// In th, this message translates to:
  /// **'ไม่พบอุปกรณ์ที่สามารถเชื่อมต่อได้ กรุณาเชื่อมต่ออุปกรณ์ใหม่และลองอีกครั้ง'**
  String get homeErrorNoDevice;

  /// แจ้งผู้ใช้ว่าไม่สามารถเชื่อมต่อเครื่องสแกนบัตรได้
  ///
  /// In th, this message translates to:
  /// **'ไม่สามารถเชื่อมต่อเครื่องสแกนบัตรได้ โปรดกดที่ไอคอนหมุนเพื่อรีเฟรชรายการแล้วลองใหม่อีกครั้ง'**
  String get homeErrorConnect;

  /// ข้อมูลบัตรประชาชน
  ///
  /// In th, this message translates to:
  /// **'ID Card Information'**
  String get cardReaderTitle;

  /// สถานะของเครื่องสแกนบัตร
  ///
  /// In th, this message translates to:
  /// **'สถานะ : '**
  String get cardReaderStatus;

  /// สถานะของเครื่องสแกนบัตรเมื่อไม่ได้เชื่อมต่อ
  ///
  /// In th, this message translates to:
  /// **'ยังไม่ได้เชื่อมต่อ'**
  String get cardReaderStatusDisconnect;

  /// สถานะของเครื่องสแกนบัตรเมื่อเชื่อมต่อแล้ว
  ///
  /// In th, this message translates to:
  /// **'เขื่อมต่อแล้ว'**
  String get cardReaderStatusConnect;

  /// ปุ่มอ่านบัตรประชาชน
  ///
  /// In th, this message translates to:
  /// **'อ่านบัตรประชาชน'**
  String get cardReaderReadSmartCardButton;

  /// ชื่อของเครื่องสแกนบัตร
  ///
  /// In th, this message translates to:
  /// **'ชื่อเครื่อง : '**
  String get cardReaderDeviceName;

  /// รหัสบัตรประชาชนของลูกค้า
  ///
  /// In th, this message translates to:
  /// **'รหัสบัตรประชาชน'**
  String get cardReaderCitizenID;

  /// คำนำหน้าชื่อของลูกค้า
  ///
  /// In th, this message translates to:
  /// **'คำนำหน้าชื่อ (ไทย)'**
  String get cardReaderThPrefix;

  /// ชื่อจริงของลูกค้า ภาษาไทย
  ///
  /// In th, this message translates to:
  /// **'ชื่อจริง (ไทย)'**
  String get cardReaderThFirstName;

  /// นามสกุลของลูกค้า ภาษาไทย
  ///
  /// In th, this message translates to:
  /// **'นามสกุล (ไทย)'**
  String get cardReaderThLastName;

  /// คำนำหน้าลูกค้า ภาษาอังกฤษ
  ///
  /// In th, this message translates to:
  /// **'คำนำหน้าชื่อ (อังกฤษ)'**
  String get cardReaderEnPrefix;

  /// ชื่อจริงของลูกค้า ภาษาอังกฤษ
  ///
  /// In th, this message translates to:
  /// **'ชื่อจริง (อังกฤษ)'**
  String get cardReaderEnFirstName;

  /// นามสกุลของลูกค้า ภาษาอังกฤษ
  ///
  /// In th, this message translates to:
  /// **'นามสกุล (อังกฤษ)'**
  String get cardReaderEnLastName;

  /// Customer's Gender
  ///
  /// In th, this message translates to:
  /// **'เพศ'**
  String get cardReaderGender;

  /// วันเกิดของลูกค้า
  ///
  /// In th, this message translates to:
  /// **'วันเกิด'**
  String get cardReaderBirthDate;

  /// ที่อยู่ของลูกค้า
  ///
  /// In th, this message translates to:
  /// **'ที่อยู่เต็ม'**
  String get cardReaderAddress;

  /// ตำบลของลูกค้า
  ///
  /// In th, this message translates to:
  /// **'ตำบล'**
  String get cardReaderSubDistricts;

  /// อำเภอของลูกค้า
  ///
  /// In th, this message translates to:
  /// **'อำเภอ'**
  String get cardReaderDistricts;

  /// จังหวัดของลูกค้า
  ///
  /// In th, this message translates to:
  /// **'จังหวัด'**
  String get cardReaderProvinces;

  /// Open ICS link
  ///
  /// In th, this message translates to:
  /// **'เปิดหน้า ICS'**
  String get cardReaderOpenICSLink;

  /// ข้อความสำเร็จ
  ///
  /// In th, this message translates to:
  /// **'สำเร็จ'**
  String get successNoErrorTxt;

  /// ข้อความเกิดข้อผิดพลาดภายใน โปรดลองอีกครั้งหรือติดต่อผู้พัฒนา
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดภายใน โปรดลองอีกครั้งหรือติดต่อผู้พัฒนา'**
  String get internalError;

  /// ข้อความใบอนุญาตไม่ถูกต้อง
  ///
  /// In th, this message translates to:
  /// **'ใบอนุญาตไม่ถูกต้อง โปรดลองอีกครั้ง'**
  String get invalidLicense;

  /// ข้อความบอกผู้ใช้ว่าไม่พบเครื่องสแกนบัตร
  ///
  /// In th, this message translates to:
  /// **'ไม่พบเครื่องสแกนบัตร โปรดตรวจสอบว่าอุปกรณ์ของคุณเปิดหรือไม่ แล้วลองอีกครั้ง'**
  String get readerNotFound;

  /// ข้อความเกิดข้อผิดพลาดการเชื่อมต่อ
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดการเชื่อมต่ออุปกรณ์สแกน โปรดกดที่ไอคอนหมุนเพื่อรีเฟรชรายการแล้วลองใหม่อีกครั้ง'**
  String get connectionError;

  /// ข้อความเกิดข้อผิดพลาดการดึงรูปภาพ
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดระหว่างการดึงรูปภาพจากบัตรประชาชน โปรดลองอีกครั้ง'**
  String get getPhotoError;

  /// ข้อความเกิดข้อผิดพลาดการดึงข้อมูลส่วนตัว
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดระหว่างการดึงข้อมูลส่วนตัวจากบัตรประชาชน โปรดลองอีกครั้ง'**
  String get getPersonalInfoTxtError;

  /// ข้อความบอกผู้ใช้ว่าบัตรไม่ถูกต้อง
  ///
  /// In th, this message translates to:
  /// **'บัตรไม่ถูกต้อง โปรดตรวจสอบว่าเป็นบัตรประชาชนที่ถูกต้องหรือไม่แล้วลองอีกครั้ง'**
  String get invalidCard;

  /// ข้อความบอกผู้ใช้ว่าเวอร์ชั่นบัตรไม่รู้จัก
  ///
  /// In th, this message translates to:
  /// **'ไม่รู้จักเวอร์ชั่นสมาร์ทการ์ดของบัตรประชาชนนี้ โปรดลองอีกครั้ง'**
  String get unknownCardVersion;

  /// ข้อความเกิดข้อผิดพลาดการตัดการเชื่อมต่อ
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดการตัดการเชื่อมต่ออุปกรณ์สแกน โปรดลองอีกครั้ง'**
  String get disconnectionError;

  /// ข้อความเกิดข้อผิดพลาดการเริ่มต้น
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดในการเริ่มต้นการเชื่อมต่อ โปรดลองอีกครั้งหรือติดต่อผู้พัฒนา'**
  String get initError;

  /// ข้อความบอกผู้ใช้ว่าไม่รองรับเครื่องสแกนบัตร
  ///
  /// In th, this message translates to:
  /// **'ไม่รองรับเครื่องสแกนบัตร โปรดลองอีกครั้งหรือติดต่อผู้พัฒนา'**
  String get readerNotSupportedError;

  /// ข้อความเกิดข้อผิดพลาดการอ่านไฟล์ใบอนุญาต
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดในการอ่านไฟล์ใบอนุญาต โปรดลองอีกครั้ง'**
  String get licenseFileError;

  /// ข้อความเกิดข้อผิดพลาดพารามิเตอร์
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดในพารามิเตอร์ โปรดลองอีกครั้ง หากยังเกิดข้อผิดพลาดโปรดติดต่อผู้พัฒนา'**
  String get parameterError;

  /// ข้อความเกิดข้อผิดพลาดการเชื่อมต่ออินเทอร์เน็ต
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดการเชื่อมต่ออินเทอร์เน็ต โปรดตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง'**
  String get internetError;

  /// ข้อความบอกผู้ใช้ว่าไม่พบบัตร
  ///
  /// In th, this message translates to:
  /// **'ไม่พบบัตร โปรดตรวจสอบว่าเป็นบัตรประชาชนที่ถูกต้องหรือไม่แล้วลองอีกครั้ง'**
  String get cardNotFoundError;

  /// ข้อความเกิดข้อผิดพลาดการอัปเดตใบอนุญาต
  ///
  /// In th, this message translates to:
  /// **'เกิดข้อผิดพลาดในการอัปเดตใบอนุญาต โปรดลองอีกครั้ง'**
  String get licenseUpdateError;

  /// ข้อความบอกผู้ใช้ว่าอัปเดตใบอนุญาตสำเร็จ
  ///
  /// In th, this message translates to:
  /// **'อัปเดตใบอนุญาตสำเร็จ โปรดกดปุ่มรีเฟรชแล้วลองเชื่อมต่อใหม่อีกครั้ง'**
  String get licenseUpdateSuccess;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['en', 'th'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'en': return AppLocalizationsEn();
    case 'th': return AppLocalizationsTh();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
