import 'app_localizations.dart';

/// The translations for Thai (`th`).
class AppLocalizationsTh extends AppLocalizations {
  AppLocalizationsTh([String locale = 'th']) : super(locale);

  @override
  String get title => 'แอพ iCare Card Reader';

  @override
  String get confirmTxt => 'ยืนยัน';

  @override
  String get okTxt => 'ตกลง';

  @override
  String get returnTxt => 'ย้อนกลับ';

  @override
  String get cancelTxt => 'ยกเลิก';

  @override
  String get errorTxt => 'ผิดพลาด';

  @override
  String get retryTxt => 'ลองอีกครั้ง';

  @override
  String get closeTxt => 'ปิด';

  @override
  String get refreshTxt => 'รีเฟรช';

  @override
  String get male => 'ชาย';

  @override
  String get female => 'หญิง';

  @override
  String get homeTitle => 'ยินดีต้อนรับสู่,\nICS iCare Card Reader';

  @override
  String get homeConnect => 'เชื่อมต่อ';

  @override
  String get homeErrorNoDevice => 'ไม่พบอุปกรณ์ที่สามารถเชื่อมต่อได้ กรุณาเชื่อมต่ออุปกรณ์ใหม่และลองอีกครั้ง';

  @override
  String get homeErrorConnect => 'ไม่สามารถเชื่อมต่อเครื่องสแกนบัตรได้ โปรดกดที่ไอคอนหมุนเพื่อรีเฟรชรายการแล้วลองใหม่อีกครั้ง';

  @override
  String get cardReaderTitle => 'ID Card Information';

  @override
  String get cardReaderStatus => 'สถานะ : ';

  @override
  String get cardReaderStatusDisconnect => 'ยังไม่ได้เชื่อมต่อ';

  @override
  String get cardReaderStatusConnect => 'เขื่อมต่อแล้ว';

  @override
  String get cardReaderReadSmartCardButton => 'อ่านบัตรประชาชน';

  @override
  String get cardReaderDeviceName => 'ชื่อเครื่อง : ';

  @override
  String get cardReaderCitizenID => 'รหัสบัตรประชาชน';

  @override
  String get cardReaderThPrefix => 'คำนำหน้าชื่อ (ไทย)';

  @override
  String get cardReaderThFirstName => 'ชื่อจริง (ไทย)';

  @override
  String get cardReaderThLastName => 'นามสกุล (ไทย)';

  @override
  String get cardReaderEnPrefix => 'คำนำหน้าชื่อ (อังกฤษ)';

  @override
  String get cardReaderEnFirstName => 'ชื่อจริง (อังกฤษ)';

  @override
  String get cardReaderEnLastName => 'นามสกุล (อังกฤษ)';

  @override
  String get cardReaderGender => 'เพศ';

  @override
  String get cardReaderBirthDate => 'วันเกิด';

  @override
  String get cardReaderAddress => 'ที่อยู่เต็ม';

  @override
  String get cardReaderSubDistricts => 'ตำบล';

  @override
  String get cardReaderDistricts => 'อำเภอ';

  @override
  String get cardReaderProvinces => 'จังหวัด';

  @override
  String get cardReaderOpenICSLink => 'เปิดหน้า ICS';

  @override
  String get successNoErrorTxt => 'สำเร็จ';

  @override
  String get internalError => 'เกิดข้อผิดพลาดภายใน โปรดลองอีกครั้งหรือติดต่อผู้พัฒนา';

  @override
  String get invalidLicense => 'ใบอนุญาตไม่ถูกต้อง โปรดลองอีกครั้ง';

  @override
  String get readerNotFound => 'ไม่พบเครื่องสแกนบัตร โปรดตรวจสอบว่าอุปกรณ์ของคุณเปิดหรือไม่ แล้วลองอีกครั้ง';

  @override
  String get connectionError => 'เกิดข้อผิดพลาดการเชื่อมต่ออุปกรณ์สแกน โปรดกดที่ไอคอนหมุนเพื่อรีเฟรชรายการแล้วลองใหม่อีกครั้ง';

  @override
  String get getPhotoError => 'เกิดข้อผิดพลาดระหว่างการดึงรูปภาพจากบัตรประชาชน โปรดลองอีกครั้ง';

  @override
  String get getPersonalInfoTxtError => 'เกิดข้อผิดพลาดระหว่างการดึงข้อมูลส่วนตัวจากบัตรประชาชน โปรดลองอีกครั้ง';

  @override
  String get invalidCard => 'บัตรไม่ถูกต้อง โปรดตรวจสอบว่าเป็นบัตรประชาชนที่ถูกต้องหรือไม่แล้วลองอีกครั้ง';

  @override
  String get unknownCardVersion => 'ไม่รู้จักเวอร์ชั่นสมาร์ทการ์ดของบัตรประชาชนนี้ โปรดลองอีกครั้ง';

  @override
  String get disconnectionError => 'เกิดข้อผิดพลาดการตัดการเชื่อมต่ออุปกรณ์สแกน โปรดลองอีกครั้ง';

  @override
  String get initError => 'เกิดข้อผิดพลาดในการเริ่มต้นการเชื่อมต่อ โปรดลองอีกครั้งหรือติดต่อผู้พัฒนา';

  @override
  String get readerNotSupportedError => 'ไม่รองรับเครื่องสแกนบัตร โปรดลองอีกครั้งหรือติดต่อผู้พัฒนา';

  @override
  String get licenseFileError => 'เกิดข้อผิดพลาดในการอ่านไฟล์ใบอนุญาต โปรดลองอีกครั้ง';

  @override
  String get parameterError => 'เกิดข้อผิดพลาดในพารามิเตอร์ โปรดลองอีกครั้ง หากยังเกิดข้อผิดพลาดโปรดติดต่อผู้พัฒนา';

  @override
  String get internetError => 'เกิดข้อผิดพลาดการเชื่อมต่ออินเทอร์เน็ต โปรดตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง';

  @override
  String get cardNotFoundError => 'ไม่พบบัตร โปรดตรวจสอบว่าเป็นบัตรประชาชนที่ถูกต้องหรือไม่แล้วลองอีกครั้ง';

  @override
  String get licenseUpdateError => 'เกิดข้อผิดพลาดในการอัปเดตใบอนุญาต โปรดลองอีกครั้ง';

  @override
  String get licenseUpdateSuccess => 'อัปเดตใบอนุญาตสำเร็จ โปรดกดปุ่มรีเฟรชแล้วลองเชื่อมต่อใหม่อีกครั้ง';
}
