import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:thai_card_reader_flutter/const/app_enum.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_info.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_repository.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_service.dart';
import 'package:thai_card_reader_flutter/features/card_reader/presentation/state/card_reader_state.dart';
import 'package:thai_card_reader_flutter/features/home/presentation/state/home_picker_state.dart';
import 'package:thai_card_reader_flutter/generated/assets.gen.dart';
import 'package:thai_card_reader_flutter/shared/app_l10n_repository.dart';
import 'package:thai_card_reader_flutter/shared/async_value_widget.dart';
import 'package:thai_card_reader_flutter/shared/background_view.dart';

class CardReaderView extends ConsumerStatefulWidget {
  const CardReaderView({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _CardReaderViewState();
}

class _CardReaderViewState extends ConsumerState<CardReaderView>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final device = ModalRoute.of(context)?.settings.arguments as String;
    final isCardInserted = ref.watch(cardReaderStatusStateProvider(device));
    final cardReaderDataState = ref.watch(cardReaderDataStateProvider);
    final cardReaderProgressState = ref.watch(cardReaderProgressStateProvider);
    final translation = context.localization;

    List<Widget> customerInfoList(CardInfoResult cardInfo) => [
          Padding(
            padding: EdgeInsets.all(8.r),
            child: Text(
              translation.cardReaderTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 25.r,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          _CustomerImage(
            image: cardInfo.photoBytes,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderCitizenID,
            data: cardInfo.cid,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderThPrefix,
            data: cardInfo.thPrefix,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderThFirstName,
            data: cardInfo.thFirstName,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderThLastName,
            data: cardInfo.thLastName,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderEnPrefix,
            data: cardInfo.enPrefix,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderEnFirstName,
            data: cardInfo.enFirstName,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderEnLastName,
            data: cardInfo.enLastName,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderBirthDate,
            data: cardInfo.birthDay.toString(),
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderGender,
            data: cardInfo.gender?.name ?? "",
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderAddress,
            data: cardInfo.address,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderSubDistricts,
            data: cardInfo.addrSubDistrict,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderDistricts,
            data: cardInfo.addrDistrict,
          ),
          _CustomerCardInfoDataText(
            title: translation.cardReaderProvinces,
            data: cardInfo.addrProvince,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CupertinoButton.filled(
                child: RichText(
                  text: TextSpan(
                    children: [
                      const WidgetSpan(
                          child: Icon(CupertinoIcons.arrow_turn_up_right),
                          alignment: PlaceholderAlignment.middle),
                      TextSpan(text: translation.cardReaderOpenICSLink),
                    ],
                  ),
                ),
                onPressed: () async {
                  await ref.read(cardReaderServiceProvider).openUrl(cardInfo);
                }),
          ),
        ];

    Future<void> showErrorDialog(String title, String content) async {
      await showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(title),
            content: Text(content),
            actions: [
              CupertinoDialogAction(
                child: Text(translation.okTxt),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    ref.listen(
      cardReaderDataStateProvider,
      (prev, next) {
        ///Error Handling
        if (next is AsyncError) {
          showErrorDialog(
            translation.errorTxt,
            next.error.toString(),
          );
        }
      },
    );

    ref.listen(
      cardReaderStatusStateProvider(device),
      (prev, next) {
        if (next.value == false && !next.isLoading) {
          ref.read(cardReaderDataStateProvider.notifier).unloadData();
        }
      },
    );

    return ColoredBox(
      color: CupertinoColors.systemGrey6,
      child: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          physics: const ClampingScrollPhysics(),
          child: ICSBackgroundView(
            child: Column(
              children: [
                LayoutBuilder(
                  builder: (context, constraints) {
                    bool useDesktopLayout =
                        constraints.maxWidth > ScreenType.tablet.width;
                    return AsyncValueWidget(
                      value: isCardInserted,
                      data: (cardInsertStatus) => Visibility(
                        visible: useDesktopLayout,
                        replacement: _ICSSmallCardReader(
                          reader: device,
                          isCardConnect: cardInsertStatus,
                        ),
                        child: _ICSCardReader(
                          reader: device,
                          isCardConnect: cardInsertStatus,
                        ),
                      ),
                      loading: () => Visibility(
                        visible: useDesktopLayout,
                        replacement: _ICSSmallCardReader(
                          reader: device,
                          isCardConnect: false,
                        ),
                        child: _ICSCardReader(
                          reader: device,
                          isCardConnect: false,
                        ),
                      ),
                      error: (e, st) => Visibility(
                        visible: useDesktopLayout,
                        replacement: _ICSSmallCardReader(
                          reader: device,
                          isCardConnect: false,
                        ),
                        child: _ICSCardReader(
                          reader: device,
                          isCardConnect: false,
                        ),
                      ),
                    );
                  },
                ),
                LayoutBuilder(
                  builder: (context, constraints) {
                    bool useDesktopLayout =
                        constraints.maxWidth > ScreenType.tablet.width;
                    return AsyncValueWidget(
                      value: cardReaderDataState,
                      data: (data) => Visibility(
                        visible: useDesktopLayout,
                        replacement: _ICSCardInfo(
                          reader: device,
                          outerContainerPadding: 20.r,
                          shouldVisible: cardReaderDataState.isLoading || data.cid.isNotEmpty,
                          children: customerInfoList(data),
                        ),
                        child: _ICSCardInfo(
                          reader: device,
                          outerContainerPadding: 40.r,
                          shouldVisible: cardReaderDataState.isLoading || data.cid.isNotEmpty,
                          children: customerInfoList(data),
                        ),
                      ),
                      loading: () => AsyncValueWidget(
                        value: cardReaderProgressState,
                        data: (progress) {
                          return SizedBox(
                            width: 400,
                            height: 400,
                            child: CircularProgressIndicator.adaptive(
                              /// progress is nullable because Windows doesn't have loading progress
                              value: (progress != null || progress != 0)
                                  ? (progress! / 100).toDouble()
                                  : .5,
                            ),
                          );
                        },
                      ),
                      error: (e, st) => Visibility(
                        visible: useDesktopLayout,
                        replacement: _ICSCardInfo(
                          reader: device,
                          outerContainerPadding: 20.r,
                          children: const [],
                        ),
                        child: _ICSCardInfo(
                          reader: device,
                          outerContainerPadding: 40.r,
                          children: const [],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _ICSSmallCardReader extends ConsumerWidget {
  const _ICSSmallCardReader({
    required this.reader,
    required this.isCardConnect,
  });

  final String reader;
  final bool isCardConnect;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final cardReaderState = ref.watch(cardReaderStateProvider);
    final translation = context.localization;

    return AsyncValueWidget(
      value: cardReaderState,
      data: (jsonRpc) => Container(
        padding: EdgeInsets.all(20.r),
        alignment: Alignment.centerLeft,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              constraints: BoxConstraints(
                  minHeight: 86.h,
                  minWidth: MediaQuery.of(context).size.width / 2),
              decoration: BoxDecoration(
                color: CupertinoColors.systemGrey6,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Wrap(
                alignment: WrapAlignment.center,
                runAlignment: WrapAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 4.0.h,
                      horizontal: 8.0.r,
                    ),
                    child: Text(
                      '${translation.cardReaderStatus} : ${isCardConnect ? translation.cardReaderStatusConnect : translation.cardReaderStatusDisconnect}',
                      softWrap: true,
                      overflow: TextOverflow.visible,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: 4.0.h, horizontal: 8.0.r),
                    child: Text(
                      '${translation.cardReaderDeviceName} : $reader',
                      softWrap: true,
                      overflow: TextOverflow.visible,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Container(
              constraints: BoxConstraints(
                  minHeight: 86.h,
                  minWidth: MediaQuery.of(context).size.width / 2),
              decoration: BoxDecoration(
                color: CupertinoColors.systemGrey6,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Wrap(
                runAlignment: WrapAlignment.center,
                alignment: WrapAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width.w / 2,
                    padding: EdgeInsets.symmetric(horizontal: 8.0.r),
                    child: CupertinoButton(
                      color: isCardConnect
                          ? CupertinoColors.systemCyan
                          : CupertinoColors.systemGrey,
                      padding: EdgeInsets.symmetric(horizontal: 8.0.r),
                      child: Text(translation.cardReaderReadSmartCardButton),
                      onPressed: () => isCardConnect
                          ? ref
                              .read(cardReaderDataStateProvider.notifier)
                              .loadData(reader)
                          : {},
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 8.0.r,
                    ),
                    child: CupertinoButton(
                      color: CupertinoColors.systemRed,
                      padding: EdgeInsets.symmetric(
                        horizontal: 8.0.r,
                      ),
                      child: const Icon(CupertinoIcons.xmark),
                      onPressed: () async {
                        bool result = true;
                        if (defaultTargetPlatform == TargetPlatform.iOS ||
                            defaultTargetPlatform == TargetPlatform.macOS) {
                          result = await ref
                              .read(cardReaderSwiftRepositoryProvider)
                              .disconnect();
                        }
                        ref.invalidate(homePickerListStateProvider);
                        ref.invalidate(homePickerTextSelectionStateProvider);
                        if (context.mounted && result) Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ICSCardReader extends ConsumerWidget {
  const _ICSCardReader({
    required this.reader,
    required this.isCardConnect,
  });

  final String reader;
  final bool isCardConnect;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final cardReaderState = ref.watch(cardReaderStateProvider);
    final translation = context.localization;

    return AsyncValueWidget(
      value: cardReaderState,
      data: (jsonRpc) => Container(
        padding: EdgeInsets.all(40.r),
        alignment: Alignment.centerLeft,
        child: Container(
          width: MediaQuery.of(context).size.width.w / 2,
          constraints: BoxConstraints(
            minHeight: 86.h,
            minWidth: 100.w,
          ),
          decoration: BoxDecoration(
            color: CupertinoColors.systemGrey6,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: 4.0.h, horizontal: 8.0.r),
                    child: Text(
                      '${translation.cardReaderStatus} : ${isCardConnect ? translation.cardReaderStatusConnect : translation.cardReaderStatusDisconnect}',
                      softWrap: true,
                      overflow: TextOverflow.visible,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: 4.0.h, horizontal: 8.0.r),
                    child: Text(
                      '${translation.cardReaderDeviceName} : $reader',
                      softWrap: true,
                      overflow: TextOverflow.visible,
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 8.0.r),
                  alignment: Alignment.centerRight,
                  child: CupertinoButton(
                    color: isCardConnect
                        ? CupertinoColors.systemCyan
                        : CupertinoColors.systemGrey,
                    padding: EdgeInsets.symmetric(horizontal: 8.0.r),
                    child: Text(translation.cardReaderReadSmartCardButton),
                    onPressed: () => isCardConnect
                        ? ref
                            .read(cardReaderDataStateProvider.notifier)
                            .loadData(reader)
                        : {},
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0.r),
                alignment: Alignment.centerRight,
                child: CupertinoButton(
                  color: CupertinoColors.systemRed,
                  padding: EdgeInsets.symmetric(horizontal: 8.0.r),
                  child: const Icon(CupertinoIcons.xmark),
                  onPressed: () {
                    ref.invalidate(homePickerListStateProvider);
                    ref.invalidate(homePickerTextSelectionStateProvider);
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ICSCardInfo extends ConsumerWidget {
  const _ICSCardInfo({
    required this.reader,
    required this.children,
    required this.outerContainerPadding,
    this.shouldVisible = false,
  });

  final String reader;
  final List<Widget> children;
  final double outerContainerPadding;
  final bool shouldVisible;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: EdgeInsets.all(outerContainerPadding),
      child: Visibility(
        visible: shouldVisible,
        child: Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: CupertinoColors.systemGrey6,
            borderRadius: BorderRadius.circular(10),
          ),
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height.h,
            maxHeight: double.infinity,
          ),
          child: ListView(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: children,
          ),
        ),
      ),
    );
  }
}

class _CustomerImage extends StatelessWidget {
  const _CustomerImage({
    required this.image,
  });

  final Uint8List? image;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0.r),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5.r),
        child: Center(
          child: Image.memory(
            image ?? Uint8List(0),
            errorBuilder: (context, error, stackTrace) {
              debugPrint('Error loading image $error');
              return Image.asset(Assets.design.download.path);
            },
          ),
        ),
      ),
    );
  }
}

class _CustomerCardInfoDataText extends ConsumerWidget {
  const _CustomerCardInfoDataText({
    required this.title,
    required this.data,
  });

  final String title;
  final String data;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 5.r,
      ),
      margin: EdgeInsets.symmetric(horizontal: 12.r, vertical: 8.h),
      width: MediaQuery.of(context).size.width.w,
      decoration: BoxDecoration(
        color: CupertinoColors.white,
        borderRadius: BorderRadius.circular(5.r),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.0.w, vertical: 8.r),
        child: Wrap(
          direction: Axis.horizontal,
          alignment: WrapAlignment.spaceBetween,
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: 10.w,
          children: [
            Text(
              title,
              softWrap: true,
              overflow: TextOverflow.visible,
            ),
            Text(
              data,
              softWrap: true,
              overflow: TextOverflow.visible,
            ),
          ],
        ),
      ),
    );
  }
}
