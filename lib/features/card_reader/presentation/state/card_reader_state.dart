import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_info.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_repository.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_service.dart';

part 'card_reader_state.g.dart';

@riverpod
class CardReaderState extends _$CardReaderState {
  @override
  Future<CardReaderRepository> build() async {
    final cardReaderRepository = ref.watch(cardReaderRepositoryProvider);
    return cardReaderRepository;
  }
}

@riverpod
class CardReaderStatusState extends _$CardReaderStatusState {
  @override
  Stream<bool> build(String reader) {
    final cardReaderRepository = ref.watch(cardReaderRepositoryProvider);
    return cardReaderRepository.isCardInserted(reader);
  }
}

@riverpod
class CardReaderDataState extends _$CardReaderDataState {
  @override
  Future<CardInfoResult> build() async {
    return CardInfoResult();
  }

  Future loadData(String reader) async {
    try {
      state = const AsyncValue.loading();
      final cardReaderRepository = ref.watch(cardReaderRepositoryProvider);
      final cardReaderServices = ref.watch(cardReaderServiceProvider);
      final data = await cardReaderRepository.getCardInfo(reader);
      state =
          await AsyncValue.guard(() => (cardReaderServices.getCardInfo(data)));
    } on Exception catch (err) {
      state = AsyncValue.error(err, StackTrace.current);
    }
  }

  Future unloadData() async {
    state = const AsyncLoading();
    state = await AsyncValue.guard(() => Future.value(CardInfoResult()));
  }
}

@riverpod
class CardReaderProgressState extends _$CardReaderProgressState {
  @override
  Stream<int?> build() async* {
    if (defaultTargetPlatform == TargetPlatform.iOS) {
      final repo = ref.watch(cardReaderSwiftRepositoryProvider);
      StreamController<int> dataStream = StreamController<int>();

      repo.getPercent().listen((event) {
        dataStream.add(event);
      });

      ref.onDispose(() {
        dataStream.close();
      });

      yield* dataStream.stream;
    } else {
      yield null;
    }
  }

  double convertToDouble() {
    return state.maybeWhen(
      data: (value) {
        double percent = (value! / 100).toDouble();
        return percent;
      },
      orElse: () => 0.0,
    );
  }
}
