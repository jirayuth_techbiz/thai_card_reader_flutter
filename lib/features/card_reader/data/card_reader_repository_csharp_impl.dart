import 'dart:async';
import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rxdart/rxdart.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_info.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_repository.dart';
import 'package:thai_card_reader_flutter/shared/API/thai_card_reader_rpc.dart';

class CardReaderRepositoryCSharpImpl implements CardReaderRepository {
  final CSharpJsonRpc cardReaderWrapper;
  final Ref ref;
  CardReaderRepositoryCSharpImpl(this.ref, {required this.cardReaderWrapper});

  @override
  Future<CardInfoCSharpJson> getCardInfo(String reader) async {
    final result =
        await cardReaderWrapper.invoke(methodName: 'readCard', param: reader);

    if (result == "null") {
      throw Exception('getCardInfo: Error reading card - return null');
    }

    try {
      final jsonRes = jsonDecode(result);
      if (jsonRes is Map && jsonRes.containsKey("error")) {
        throw Exception('getCardInfo: Error reading card - ${jsonRes["error"]}');
      }
      var cardInfo = CardInfoCSharpJson.fromJson(jsonRes);
      return cardInfo;
    } catch (e) {
      throw Exception('getCardInfo: Error decoding data - $e');
    }
  }

  @override
  Stream<bool> isCardInserted(String reader) async* {
    BehaviorSubject<bool> cardStatuscontroller = BehaviorSubject<bool>();
    Timer? timer;

    timer = Timer.periodic(const Duration(seconds: 2), (timer) async {
      final result = await cardReaderWrapper.invoke(
          methodName: 'onCardInserted', param: reader);
      cardStatuscontroller.sink.add(result);
    });

    ref.onDispose(() {
      timer?.cancel();
      cardStatuscontroller.close();
      _disconnect(reader);
    });

    yield* cardStatuscontroller.stream;
  }

  Future<bool> _disconnect(String reader) async {
    final result = await cardReaderWrapper.invoke(
      methodName: 'stopReader',
      param: reader,
    );

    if (result == "null" || result == null) {
      throw Exception('_disconnect: Error disconnecting reader - return null');
    }

    return result;
  }
}
