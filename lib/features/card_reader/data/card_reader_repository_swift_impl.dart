import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:thai_card_reader_flutter/const/app_enum.dart';
import 'package:thai_card_reader_flutter/const/error_swift_enum.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_info.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_repository.dart';
import 'package:thai_card_reader_flutter/generated/app_localizations.dart';
import 'package:thai_card_reader_flutter/generated/swift_pigeon.g.dart';

class CardReaderRepositorySwiftImpl
    implements CardReaderRepository, CardReaderSwiftRepository {
  final Ref ref;
  final CardReaderSwiftAPI cardReaderSwiftAPI;
  final AppLocalizations locale;

  CardReaderRepositorySwiftImpl(this.ref, {required this.cardReaderSwiftAPI, required this.locale});

  static const _cardStatusHandlerEvent = EventChannel("cardStatusHandlerEvent");
  static const _cardCurrentlyReadHandlerEvent =
      EventChannel("cardCurrentlyReadHandlerEvent");
  static const _percentHandlerEvent = EventChannel("percentHandlerEvent");

  @override
  Future<CardInfoCSharpJson> getCardInfo(String reader) async {
    final resultText = await cardReaderSwiftAPI
        .readCardTextInfo(GetCardInfoRequest()..readerName = reader);
    if (resultText.argData != null) {
      ///argData is a long string that contains all the card information separated by a #
      String data = resultText.argData as String;
      final resultList =
          data.split('#').map((value) => value.trim()).toList().asMap();

      ///TODO
      CardInfoCSharpJson cardInfo = CardInfoCSharpJson(
        cid: resultList[CardReaderEnumSwift.cid.code]!,
        thPrefix: resultList[CardReaderEnumSwift.thPrefix.code]!,
        thFirstName: resultList[CardReaderEnumSwift.thFirstName.code]!,

        ///Middle Name is not available in Thai ID card [CardReaderEnumSwift.thMiddleName.code]
        thLastName: resultList[CardReaderEnumSwift.thLastName.code]!,
        enPrefix: resultList[CardReaderEnumSwift.enPrefix.code]!,
        enFirstName: resultList[CardReaderEnumSwift.enFirstName.code]!,

        ///Middle Name is not available in Thai ID card [CardReaderEnumSwift.enMiddleName.code]
        enLastName: resultList[CardReaderEnumSwift.enLastName.code]!,
        addrHouseNo: resultList[CardReaderEnumSwift.addrHouseNo.code]!,
        addrVillageNo:
            "${resultList[CardReaderEnumSwift.addrVillageNo.code]!} ${resultList[CardReaderEnumSwift.addrVillageName.code]!}",
        addrSoi: resultList[CardReaderEnumSwift.addrSoi.code]!,
        addrRoad: resultList[CardReaderEnumSwift.addrRoad.code]!,
        addrDistrict: resultList[CardReaderEnumSwift.addrDistrict.code]!,
        addrSubDistrict: resultList[CardReaderEnumSwift.addrSubDistrict.code]!,
        addrProvince: resultList[CardReaderEnumSwift.addrProvince.code]!,
        birthDay: resultList[CardReaderEnumSwift.birthDay.code]!,
        gender: resultList[CardReaderEnumSwift.gender.code]!,
        issuer: resultList[CardReaderEnumSwift.issuer.code]!,
        issueDate: resultList[CardReaderEnumSwift.issueDate.code]!,
        expireDate: resultList[CardReaderEnumSwift.expireDate.code]!,
        address:
            "${resultList[CardReaderEnumSwift.addrHouseNo.code]} ${resultList[CardReaderEnumSwift.addrVillageNo.code]} ${resultList[CardReaderEnumSwift.addrVillageName.code]} ${resultList[CardReaderEnumSwift.addrSoi.code]}, ${resultList[CardReaderEnumSwift.addrSubDistrict.code]} ${resultList[CardReaderEnumSwift.addrDistrict.code]} ${resultList[CardReaderEnumSwift.addrProvince.code]}",
      );

      final resultImg = await cardReaderSwiftAPI
          .readCardImage(GetCardInfoRequest()..readerName = reader);

      if (resultImg.argData != null) {
        cardInfo = cardInfo.copyWith(
          ///For compatibility with C# model
          ///
          ///Convert bytes to base64
          photoBytes: base64Encode(resultImg.argData as List<int>),
        );
      }
      return cardInfo;
    } else {
       throw Exception(CardReaderSwiftError.getErrorType(resultText.errorCode!)
            .getErrorDescription(locale));
    }
  }

  @override
  Stream<bool> isCardInserted(String reader) async* {
    final result = _cardStatusHandlerEvent.receiveBroadcastStream();
    StreamController<bool> cardReadingStatusController =
        StreamController<bool>();
    result.listen((data) {
      debugPrint(data.toString());
      cardReadingStatusController.sink.add(data as bool);
    });

    ref.onDispose(() {
      cardReadingStatusController.close();
    });

    yield* cardReadingStatusController.stream;
  }

  @override
  Stream<bool> isCardCurrentlyReading() async* {
    final result = _cardCurrentlyReadHandlerEvent.receiveBroadcastStream();
    StreamController<bool> cardCurrentlyReadingController =
        StreamController<bool>();
    result.listen((data) {
      debugPrint(data.toString());
      cardCurrentlyReadingController.sink.add(data as bool);
    });

    ref.onDispose(() {
      cardCurrentlyReadingController.close();
    });

    yield* cardCurrentlyReadingController.stream;
  }

  @override
  Stream<int> getPercent() async* {
    final result = _percentHandlerEvent.receiveBroadcastStream();
    StreamController<int> percentController = StreamController<int>();
    result.listen((data) {
      percentController.sink.add(data as int);
    });

    ref.onDispose(() {
      percentController.close();
    });

    yield* percentController.stream;
  }

  @override
  Future<bool> disconnect() async {
    final result = await cardReaderSwiftAPI.disconnect();
    if (result.errorCode != null) {
      throw Exception(CardReaderSwiftError.getErrorType(result.errorCode!)
          .getErrorDescription(locale));
    }
    return result.result!;
  }
}
