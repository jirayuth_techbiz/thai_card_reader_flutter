import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_info.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_repository.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_service.dart';
import 'package:thai_card_reader_flutter/shared/extension/tryparse_datetime_format_extension.dart';
import 'package:url_launcher/url_launcher.dart';

class CardReaderServiceImpl implements CardReaderService {
  CardReaderServiceImpl(this.cardReaderRepo);

  final CardReaderRepository cardReaderRepo;

  @override
  Future<void> openUrl(CardInfoResult cardInfo) async {
    final query =
        "?cid=${cardInfo.cid}&prefix=${cardInfo.thPrefix}&firstname=${cardInfo.thFirstName}&lastname=${cardInfo.thLastName}&district=${cardInfo.addrDistrict}&subdistrict=${cardInfo.addrSubDistrict}&province=${cardInfo.addrProvince}&address=${cardInfo.address}";
    // ignore: no_leading_underscores_for_local_identifiers
    try {
      final url = Uri.parse(
          "https://www.icare-aasp.com/Backoffice/Customer/customer-info.aspx$query");
      if (await canLaunchUrl(url)) {
        await launchUrl(url);
      } else {
        throw Exception('Could not launch $url');
      }
    } catch (e) {
      debugPrint(e.toString());
      throw Exception('URL Error');
    }
  }

  @override
  Future<CardInfoResult> getCardInfo(CardInfoCSharpJson cardInfoJson) async {
    try {
      final DateTime? birthDay =
          cardInfoJson.birthDay.tryParseDateTime();
      final String newBirthDay = DateFormat("dd MMM yyyy").format(birthDay!);
      final DateTime? expireDate = cardInfoJson.expireDate.tryParseDateTime();
      final String newExpireDate =
          DateFormat("dd MMM yyyy").format(expireDate!);
      final DateTime? issuerDate =
          cardInfoJson.issueDate.tryParseDateTime();
      final String newIssueDate = DateFormat("dd MMM yyyy").format(issuerDate!);
      final Uint8List photoBytes = base64Decode(cardInfoJson.photoBytes);
      final Gender gender = Gender.fromInt(int.parse(cardInfoJson.gender));

      return CardInfoResult(
        cid: cardInfoJson.cid,
        birthDay: newBirthDay,
        gender: gender,
        thPrefix: cardInfoJson.thPrefix,
        thFirstName: cardInfoJson.thFirstName,
        thLastName: cardInfoJson.thLastName,
        enPrefix: cardInfoJson.enPrefix,
        enFirstName: cardInfoJson.enFirstName,
        enLastName: cardInfoJson.enLastName,
        address: cardInfoJson.address,
        addrHouseNo: cardInfoJson.addrHouseNo,
        addrVillageNo: cardInfoJson.addrVillageNo,
        addrSoi: cardInfoJson.addrSoi,
        addrRoad: cardInfoJson.addrRoad,
        addrSubDistrict: cardInfoJson.addrSubDistrict,
        addrDistrict: cardInfoJson.addrDistrict,
        addrProvince: cardInfoJson.addrProvince,
        issueDate: newIssueDate,
        expireDate: newExpireDate,
        photoBytes: photoBytes,
      );
    } catch (e) {
      debugPrint(e.toString());
      throw Exception('Error parsing card info : $e');
    }
  }
}

