import 'dart:typed_data';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'card_info.freezed.dart';
part 'card_info.g.dart';

@freezed
class CardInfoCSharpJson with _$CardInfoCSharpJson {
  const factory CardInfoCSharpJson({
    @Default("") @JsonKey(name: "Citizenid") String cid,
    @Default("") @JsonKey(name: "Birthday") String birthDay,
    @Default("") @JsonKey(name: "Sex") String gender,
    @Default("") @JsonKey(name: "Th_Prefix") String thPrefix,
    @Default("") @JsonKey(name: "Th_Firstname") String thFirstName,
    @Default("") @JsonKey(name: "Th_Lastname") String thLastName,
    @Default("") @JsonKey(name: "En_Prefix") String enPrefix,
    @Default("") @JsonKey(name: "En_Firstname") String enFirstName,
    @Default("") @JsonKey(name: "En_Lastname") String enLastName,
    @Default("") @JsonKey(name: "Address") String address,
    @Default("") @JsonKey(name: "addrHouseNo") String addrHouseNo, // บ้านเลขที่
    @Default("") @JsonKey(name: "addrVillageNo") String addrVillageNo, // หมู่ที่
    @Default("") @JsonKey(name: "addrLane") String addrSoi, // ซอย
    @Default("") @JsonKey(name: "addrRoad") String addrRoad, // ถนน
    @Default("") @JsonKey(name: "addrTambol") String addrSubDistrict, // ตำบล
    @Default("") @JsonKey(name: "addrAmphur") String addrDistrict, // อำเภอ
    @Default("") @JsonKey(name: "addrProvince") String addrProvince, // จังหวัด
    @Default("") @JsonKey(name: "Issuer") String issuer, // ผู้ออกบัตร
    @Default("") @JsonKey(name: "Issue") String issueDate, // วันออกบัตร
    @Default("") @JsonKey(name: "Issue_Expire") String expireDate, // วันหมดอายุ
    @Default("") @JsonKey(name: "PhotoRaw") String photoBytes, // รูปภาพ
  }) = _CardInfoCSharpJson;

  const CardInfoCSharpJson._();

  factory CardInfoCSharpJson.fromJson(Map<String, dynamic> json) =>
      _$CardInfoCSharpJsonFromJson(json);
} 

@unfreezed
class CardInfoResult with _$CardInfoResult {
  factory CardInfoResult({
    @Default("") String cid,
    @Default("") String birthDay,
    Gender? gender,
    @Default("") String thPrefix,
    @Default("") String thFirstName,
    @Default("") String thLastName,
    @Default("") String enPrefix,
    @Default("") String enFirstName,
    @Default("") String enLastName,
    @Default("") String address,
    @Default("") String addrHouseNo, // บ้านเลขที่
    @Default("") String addrVillageNo, // หมู่ที่
    @Default("") String addrSoi, // ซอย
    @Default("") String addrRoad, // ถนน
    @Default("") String addrSubDistrict, // ตำบล
    @Default("") String addrDistrict, // อำเภอ
    @Default("") String addrProvince, // จังหวัด
    @Default("") String issueDate, // วันออกบัตร
    @Default("") String expireDate, // วันหมดอายุ
    Uint8List? photoBytes, // รูปภาพ
  }) = _CardInfoResult;

  const CardInfoResult._();
}

enum Gender {
  male(1, "Male"),
  female(2, "Female");

  final int value;
  final String name;

  const Gender(this.value, this.name);

  static Gender fromInt(int value) {
    return Gender.values.firstWhere(
      (element) => element.value == value,
      orElse: () => throw ArgumentError('Invalid Gender'),
    );
  }
}
