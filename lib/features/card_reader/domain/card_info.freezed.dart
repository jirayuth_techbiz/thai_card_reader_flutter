// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'card_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

CardInfoCSharpJson _$CardInfoCSharpJsonFromJson(Map<String, dynamic> json) {
  return _CardInfoCSharpJson.fromJson(json);
}

/// @nodoc
mixin _$CardInfoCSharpJson {
  @JsonKey(name: "Citizenid")
  String get cid => throw _privateConstructorUsedError;
  @JsonKey(name: "Birthday")
  String get birthDay => throw _privateConstructorUsedError;
  @JsonKey(name: "Sex")
  String get gender => throw _privateConstructorUsedError;
  @JsonKey(name: "Th_Prefix")
  String get thPrefix => throw _privateConstructorUsedError;
  @JsonKey(name: "Th_Firstname")
  String get thFirstName => throw _privateConstructorUsedError;
  @JsonKey(name: "Th_Lastname")
  String get thLastName => throw _privateConstructorUsedError;
  @JsonKey(name: "En_Prefix")
  String get enPrefix => throw _privateConstructorUsedError;
  @JsonKey(name: "En_Firstname")
  String get enFirstName => throw _privateConstructorUsedError;
  @JsonKey(name: "En_Lastname")
  String get enLastName => throw _privateConstructorUsedError;
  @JsonKey(name: "Address")
  String get address => throw _privateConstructorUsedError;
  @JsonKey(name: "addrHouseNo")
  String get addrHouseNo => throw _privateConstructorUsedError; // บ้านเลขที่
  @JsonKey(name: "addrVillageNo")
  String get addrVillageNo => throw _privateConstructorUsedError; // หมู่ที่
  @JsonKey(name: "addrLane")
  String get addrSoi => throw _privateConstructorUsedError; // ซอย
  @JsonKey(name: "addrRoad")
  String get addrRoad => throw _privateConstructorUsedError; // ถนน
  @JsonKey(name: "addrTambol")
  String get addrSubDistrict => throw _privateConstructorUsedError; // ตำบล
  @JsonKey(name: "addrAmphur")
  String get addrDistrict => throw _privateConstructorUsedError; // อำเภอ
  @JsonKey(name: "addrProvince")
  String get addrProvince => throw _privateConstructorUsedError; // จังหวัด
  @JsonKey(name: "Issuer")
  String get issuer => throw _privateConstructorUsedError; // ผู้ออกบัตร
  @JsonKey(name: "Issue")
  String get issueDate => throw _privateConstructorUsedError; // วันออกบัตร
  @JsonKey(name: "Issue_Expire")
  String get expireDate => throw _privateConstructorUsedError; // วันหมดอายุ
  @JsonKey(name: "PhotoRaw")
  String get photoBytes => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CardInfoCSharpJsonCopyWith<CardInfoCSharpJson> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CardInfoCSharpJsonCopyWith<$Res> {
  factory $CardInfoCSharpJsonCopyWith(
          CardInfoCSharpJson value, $Res Function(CardInfoCSharpJson) then) =
      _$CardInfoCSharpJsonCopyWithImpl<$Res, CardInfoCSharpJson>;
  @useResult
  $Res call(
      {@JsonKey(name: "Citizenid") String cid,
      @JsonKey(name: "Birthday") String birthDay,
      @JsonKey(name: "Sex") String gender,
      @JsonKey(name: "Th_Prefix") String thPrefix,
      @JsonKey(name: "Th_Firstname") String thFirstName,
      @JsonKey(name: "Th_Lastname") String thLastName,
      @JsonKey(name: "En_Prefix") String enPrefix,
      @JsonKey(name: "En_Firstname") String enFirstName,
      @JsonKey(name: "En_Lastname") String enLastName,
      @JsonKey(name: "Address") String address,
      @JsonKey(name: "addrHouseNo") String addrHouseNo,
      @JsonKey(name: "addrVillageNo") String addrVillageNo,
      @JsonKey(name: "addrLane") String addrSoi,
      @JsonKey(name: "addrRoad") String addrRoad,
      @JsonKey(name: "addrTambol") String addrSubDistrict,
      @JsonKey(name: "addrAmphur") String addrDistrict,
      @JsonKey(name: "addrProvince") String addrProvince,
      @JsonKey(name: "Issuer") String issuer,
      @JsonKey(name: "Issue") String issueDate,
      @JsonKey(name: "Issue_Expire") String expireDate,
      @JsonKey(name: "PhotoRaw") String photoBytes});
}

/// @nodoc
class _$CardInfoCSharpJsonCopyWithImpl<$Res, $Val extends CardInfoCSharpJson>
    implements $CardInfoCSharpJsonCopyWith<$Res> {
  _$CardInfoCSharpJsonCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cid = null,
    Object? birthDay = null,
    Object? gender = null,
    Object? thPrefix = null,
    Object? thFirstName = null,
    Object? thLastName = null,
    Object? enPrefix = null,
    Object? enFirstName = null,
    Object? enLastName = null,
    Object? address = null,
    Object? addrHouseNo = null,
    Object? addrVillageNo = null,
    Object? addrSoi = null,
    Object? addrRoad = null,
    Object? addrSubDistrict = null,
    Object? addrDistrict = null,
    Object? addrProvince = null,
    Object? issuer = null,
    Object? issueDate = null,
    Object? expireDate = null,
    Object? photoBytes = null,
  }) {
    return _then(_value.copyWith(
      cid: null == cid
          ? _value.cid
          : cid // ignore: cast_nullable_to_non_nullable
              as String,
      birthDay: null == birthDay
          ? _value.birthDay
          : birthDay // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      thPrefix: null == thPrefix
          ? _value.thPrefix
          : thPrefix // ignore: cast_nullable_to_non_nullable
              as String,
      thFirstName: null == thFirstName
          ? _value.thFirstName
          : thFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      thLastName: null == thLastName
          ? _value.thLastName
          : thLastName // ignore: cast_nullable_to_non_nullable
              as String,
      enPrefix: null == enPrefix
          ? _value.enPrefix
          : enPrefix // ignore: cast_nullable_to_non_nullable
              as String,
      enFirstName: null == enFirstName
          ? _value.enFirstName
          : enFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      enLastName: null == enLastName
          ? _value.enLastName
          : enLastName // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      addrHouseNo: null == addrHouseNo
          ? _value.addrHouseNo
          : addrHouseNo // ignore: cast_nullable_to_non_nullable
              as String,
      addrVillageNo: null == addrVillageNo
          ? _value.addrVillageNo
          : addrVillageNo // ignore: cast_nullable_to_non_nullable
              as String,
      addrSoi: null == addrSoi
          ? _value.addrSoi
          : addrSoi // ignore: cast_nullable_to_non_nullable
              as String,
      addrRoad: null == addrRoad
          ? _value.addrRoad
          : addrRoad // ignore: cast_nullable_to_non_nullable
              as String,
      addrSubDistrict: null == addrSubDistrict
          ? _value.addrSubDistrict
          : addrSubDistrict // ignore: cast_nullable_to_non_nullable
              as String,
      addrDistrict: null == addrDistrict
          ? _value.addrDistrict
          : addrDistrict // ignore: cast_nullable_to_non_nullable
              as String,
      addrProvince: null == addrProvince
          ? _value.addrProvince
          : addrProvince // ignore: cast_nullable_to_non_nullable
              as String,
      issuer: null == issuer
          ? _value.issuer
          : issuer // ignore: cast_nullable_to_non_nullable
              as String,
      issueDate: null == issueDate
          ? _value.issueDate
          : issueDate // ignore: cast_nullable_to_non_nullable
              as String,
      expireDate: null == expireDate
          ? _value.expireDate
          : expireDate // ignore: cast_nullable_to_non_nullable
              as String,
      photoBytes: null == photoBytes
          ? _value.photoBytes
          : photoBytes // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CardInfoCSharpJsonImplCopyWith<$Res>
    implements $CardInfoCSharpJsonCopyWith<$Res> {
  factory _$$CardInfoCSharpJsonImplCopyWith(_$CardInfoCSharpJsonImpl value,
          $Res Function(_$CardInfoCSharpJsonImpl) then) =
      __$$CardInfoCSharpJsonImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "Citizenid") String cid,
      @JsonKey(name: "Birthday") String birthDay,
      @JsonKey(name: "Sex") String gender,
      @JsonKey(name: "Th_Prefix") String thPrefix,
      @JsonKey(name: "Th_Firstname") String thFirstName,
      @JsonKey(name: "Th_Lastname") String thLastName,
      @JsonKey(name: "En_Prefix") String enPrefix,
      @JsonKey(name: "En_Firstname") String enFirstName,
      @JsonKey(name: "En_Lastname") String enLastName,
      @JsonKey(name: "Address") String address,
      @JsonKey(name: "addrHouseNo") String addrHouseNo,
      @JsonKey(name: "addrVillageNo") String addrVillageNo,
      @JsonKey(name: "addrLane") String addrSoi,
      @JsonKey(name: "addrRoad") String addrRoad,
      @JsonKey(name: "addrTambol") String addrSubDistrict,
      @JsonKey(name: "addrAmphur") String addrDistrict,
      @JsonKey(name: "addrProvince") String addrProvince,
      @JsonKey(name: "Issuer") String issuer,
      @JsonKey(name: "Issue") String issueDate,
      @JsonKey(name: "Issue_Expire") String expireDate,
      @JsonKey(name: "PhotoRaw") String photoBytes});
}

/// @nodoc
class __$$CardInfoCSharpJsonImplCopyWithImpl<$Res>
    extends _$CardInfoCSharpJsonCopyWithImpl<$Res, _$CardInfoCSharpJsonImpl>
    implements _$$CardInfoCSharpJsonImplCopyWith<$Res> {
  __$$CardInfoCSharpJsonImplCopyWithImpl(_$CardInfoCSharpJsonImpl _value,
      $Res Function(_$CardInfoCSharpJsonImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cid = null,
    Object? birthDay = null,
    Object? gender = null,
    Object? thPrefix = null,
    Object? thFirstName = null,
    Object? thLastName = null,
    Object? enPrefix = null,
    Object? enFirstName = null,
    Object? enLastName = null,
    Object? address = null,
    Object? addrHouseNo = null,
    Object? addrVillageNo = null,
    Object? addrSoi = null,
    Object? addrRoad = null,
    Object? addrSubDistrict = null,
    Object? addrDistrict = null,
    Object? addrProvince = null,
    Object? issuer = null,
    Object? issueDate = null,
    Object? expireDate = null,
    Object? photoBytes = null,
  }) {
    return _then(_$CardInfoCSharpJsonImpl(
      cid: null == cid
          ? _value.cid
          : cid // ignore: cast_nullable_to_non_nullable
              as String,
      birthDay: null == birthDay
          ? _value.birthDay
          : birthDay // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      thPrefix: null == thPrefix
          ? _value.thPrefix
          : thPrefix // ignore: cast_nullable_to_non_nullable
              as String,
      thFirstName: null == thFirstName
          ? _value.thFirstName
          : thFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      thLastName: null == thLastName
          ? _value.thLastName
          : thLastName // ignore: cast_nullable_to_non_nullable
              as String,
      enPrefix: null == enPrefix
          ? _value.enPrefix
          : enPrefix // ignore: cast_nullable_to_non_nullable
              as String,
      enFirstName: null == enFirstName
          ? _value.enFirstName
          : enFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      enLastName: null == enLastName
          ? _value.enLastName
          : enLastName // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      addrHouseNo: null == addrHouseNo
          ? _value.addrHouseNo
          : addrHouseNo // ignore: cast_nullable_to_non_nullable
              as String,
      addrVillageNo: null == addrVillageNo
          ? _value.addrVillageNo
          : addrVillageNo // ignore: cast_nullable_to_non_nullable
              as String,
      addrSoi: null == addrSoi
          ? _value.addrSoi
          : addrSoi // ignore: cast_nullable_to_non_nullable
              as String,
      addrRoad: null == addrRoad
          ? _value.addrRoad
          : addrRoad // ignore: cast_nullable_to_non_nullable
              as String,
      addrSubDistrict: null == addrSubDistrict
          ? _value.addrSubDistrict
          : addrSubDistrict // ignore: cast_nullable_to_non_nullable
              as String,
      addrDistrict: null == addrDistrict
          ? _value.addrDistrict
          : addrDistrict // ignore: cast_nullable_to_non_nullable
              as String,
      addrProvince: null == addrProvince
          ? _value.addrProvince
          : addrProvince // ignore: cast_nullable_to_non_nullable
              as String,
      issuer: null == issuer
          ? _value.issuer
          : issuer // ignore: cast_nullable_to_non_nullable
              as String,
      issueDate: null == issueDate
          ? _value.issueDate
          : issueDate // ignore: cast_nullable_to_non_nullable
              as String,
      expireDate: null == expireDate
          ? _value.expireDate
          : expireDate // ignore: cast_nullable_to_non_nullable
              as String,
      photoBytes: null == photoBytes
          ? _value.photoBytes
          : photoBytes // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$CardInfoCSharpJsonImpl extends _CardInfoCSharpJson {
  const _$CardInfoCSharpJsonImpl(
      {@JsonKey(name: "Citizenid") this.cid = "",
      @JsonKey(name: "Birthday") this.birthDay = "",
      @JsonKey(name: "Sex") this.gender = "",
      @JsonKey(name: "Th_Prefix") this.thPrefix = "",
      @JsonKey(name: "Th_Firstname") this.thFirstName = "",
      @JsonKey(name: "Th_Lastname") this.thLastName = "",
      @JsonKey(name: "En_Prefix") this.enPrefix = "",
      @JsonKey(name: "En_Firstname") this.enFirstName = "",
      @JsonKey(name: "En_Lastname") this.enLastName = "",
      @JsonKey(name: "Address") this.address = "",
      @JsonKey(name: "addrHouseNo") this.addrHouseNo = "",
      @JsonKey(name: "addrVillageNo") this.addrVillageNo = "",
      @JsonKey(name: "addrLane") this.addrSoi = "",
      @JsonKey(name: "addrRoad") this.addrRoad = "",
      @JsonKey(name: "addrTambol") this.addrSubDistrict = "",
      @JsonKey(name: "addrAmphur") this.addrDistrict = "",
      @JsonKey(name: "addrProvince") this.addrProvince = "",
      @JsonKey(name: "Issuer") this.issuer = "",
      @JsonKey(name: "Issue") this.issueDate = "",
      @JsonKey(name: "Issue_Expire") this.expireDate = "",
      @JsonKey(name: "PhotoRaw") this.photoBytes = ""})
      : super._();

  factory _$CardInfoCSharpJsonImpl.fromJson(Map<String, dynamic> json) =>
      _$$CardInfoCSharpJsonImplFromJson(json);

  @override
  @JsonKey(name: "Citizenid")
  final String cid;
  @override
  @JsonKey(name: "Birthday")
  final String birthDay;
  @override
  @JsonKey(name: "Sex")
  final String gender;
  @override
  @JsonKey(name: "Th_Prefix")
  final String thPrefix;
  @override
  @JsonKey(name: "Th_Firstname")
  final String thFirstName;
  @override
  @JsonKey(name: "Th_Lastname")
  final String thLastName;
  @override
  @JsonKey(name: "En_Prefix")
  final String enPrefix;
  @override
  @JsonKey(name: "En_Firstname")
  final String enFirstName;
  @override
  @JsonKey(name: "En_Lastname")
  final String enLastName;
  @override
  @JsonKey(name: "Address")
  final String address;
  @override
  @JsonKey(name: "addrHouseNo")
  final String addrHouseNo;
// บ้านเลขที่
  @override
  @JsonKey(name: "addrVillageNo")
  final String addrVillageNo;
// หมู่ที่
  @override
  @JsonKey(name: "addrLane")
  final String addrSoi;
// ซอย
  @override
  @JsonKey(name: "addrRoad")
  final String addrRoad;
// ถนน
  @override
  @JsonKey(name: "addrTambol")
  final String addrSubDistrict;
// ตำบล
  @override
  @JsonKey(name: "addrAmphur")
  final String addrDistrict;
// อำเภอ
  @override
  @JsonKey(name: "addrProvince")
  final String addrProvince;
// จังหวัด
  @override
  @JsonKey(name: "Issuer")
  final String issuer;
// ผู้ออกบัตร
  @override
  @JsonKey(name: "Issue")
  final String issueDate;
// วันออกบัตร
  @override
  @JsonKey(name: "Issue_Expire")
  final String expireDate;
// วันหมดอายุ
  @override
  @JsonKey(name: "PhotoRaw")
  final String photoBytes;

  @override
  String toString() {
    return 'CardInfoCSharpJson(cid: $cid, birthDay: $birthDay, gender: $gender, thPrefix: $thPrefix, thFirstName: $thFirstName, thLastName: $thLastName, enPrefix: $enPrefix, enFirstName: $enFirstName, enLastName: $enLastName, address: $address, addrHouseNo: $addrHouseNo, addrVillageNo: $addrVillageNo, addrSoi: $addrSoi, addrRoad: $addrRoad, addrSubDistrict: $addrSubDistrict, addrDistrict: $addrDistrict, addrProvince: $addrProvince, issuer: $issuer, issueDate: $issueDate, expireDate: $expireDate, photoBytes: $photoBytes)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CardInfoCSharpJsonImpl &&
            (identical(other.cid, cid) || other.cid == cid) &&
            (identical(other.birthDay, birthDay) ||
                other.birthDay == birthDay) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.thPrefix, thPrefix) ||
                other.thPrefix == thPrefix) &&
            (identical(other.thFirstName, thFirstName) ||
                other.thFirstName == thFirstName) &&
            (identical(other.thLastName, thLastName) ||
                other.thLastName == thLastName) &&
            (identical(other.enPrefix, enPrefix) ||
                other.enPrefix == enPrefix) &&
            (identical(other.enFirstName, enFirstName) ||
                other.enFirstName == enFirstName) &&
            (identical(other.enLastName, enLastName) ||
                other.enLastName == enLastName) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.addrHouseNo, addrHouseNo) ||
                other.addrHouseNo == addrHouseNo) &&
            (identical(other.addrVillageNo, addrVillageNo) ||
                other.addrVillageNo == addrVillageNo) &&
            (identical(other.addrSoi, addrSoi) || other.addrSoi == addrSoi) &&
            (identical(other.addrRoad, addrRoad) ||
                other.addrRoad == addrRoad) &&
            (identical(other.addrSubDistrict, addrSubDistrict) ||
                other.addrSubDistrict == addrSubDistrict) &&
            (identical(other.addrDistrict, addrDistrict) ||
                other.addrDistrict == addrDistrict) &&
            (identical(other.addrProvince, addrProvince) ||
                other.addrProvince == addrProvince) &&
            (identical(other.issuer, issuer) || other.issuer == issuer) &&
            (identical(other.issueDate, issueDate) ||
                other.issueDate == issueDate) &&
            (identical(other.expireDate, expireDate) ||
                other.expireDate == expireDate) &&
            (identical(other.photoBytes, photoBytes) ||
                other.photoBytes == photoBytes));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        cid,
        birthDay,
        gender,
        thPrefix,
        thFirstName,
        thLastName,
        enPrefix,
        enFirstName,
        enLastName,
        address,
        addrHouseNo,
        addrVillageNo,
        addrSoi,
        addrRoad,
        addrSubDistrict,
        addrDistrict,
        addrProvince,
        issuer,
        issueDate,
        expireDate,
        photoBytes
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CardInfoCSharpJsonImplCopyWith<_$CardInfoCSharpJsonImpl> get copyWith =>
      __$$CardInfoCSharpJsonImplCopyWithImpl<_$CardInfoCSharpJsonImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$CardInfoCSharpJsonImplToJson(
      this,
    );
  }
}

abstract class _CardInfoCSharpJson extends CardInfoCSharpJson {
  const factory _CardInfoCSharpJson(
          {@JsonKey(name: "Citizenid") final String cid,
          @JsonKey(name: "Birthday") final String birthDay,
          @JsonKey(name: "Sex") final String gender,
          @JsonKey(name: "Th_Prefix") final String thPrefix,
          @JsonKey(name: "Th_Firstname") final String thFirstName,
          @JsonKey(name: "Th_Lastname") final String thLastName,
          @JsonKey(name: "En_Prefix") final String enPrefix,
          @JsonKey(name: "En_Firstname") final String enFirstName,
          @JsonKey(name: "En_Lastname") final String enLastName,
          @JsonKey(name: "Address") final String address,
          @JsonKey(name: "addrHouseNo") final String addrHouseNo,
          @JsonKey(name: "addrVillageNo") final String addrVillageNo,
          @JsonKey(name: "addrLane") final String addrSoi,
          @JsonKey(name: "addrRoad") final String addrRoad,
          @JsonKey(name: "addrTambol") final String addrSubDistrict,
          @JsonKey(name: "addrAmphur") final String addrDistrict,
          @JsonKey(name: "addrProvince") final String addrProvince,
          @JsonKey(name: "Issuer") final String issuer,
          @JsonKey(name: "Issue") final String issueDate,
          @JsonKey(name: "Issue_Expire") final String expireDate,
          @JsonKey(name: "PhotoRaw") final String photoBytes}) =
      _$CardInfoCSharpJsonImpl;
  const _CardInfoCSharpJson._() : super._();

  factory _CardInfoCSharpJson.fromJson(Map<String, dynamic> json) =
      _$CardInfoCSharpJsonImpl.fromJson;

  @override
  @JsonKey(name: "Citizenid")
  String get cid;
  @override
  @JsonKey(name: "Birthday")
  String get birthDay;
  @override
  @JsonKey(name: "Sex")
  String get gender;
  @override
  @JsonKey(name: "Th_Prefix")
  String get thPrefix;
  @override
  @JsonKey(name: "Th_Firstname")
  String get thFirstName;
  @override
  @JsonKey(name: "Th_Lastname")
  String get thLastName;
  @override
  @JsonKey(name: "En_Prefix")
  String get enPrefix;
  @override
  @JsonKey(name: "En_Firstname")
  String get enFirstName;
  @override
  @JsonKey(name: "En_Lastname")
  String get enLastName;
  @override
  @JsonKey(name: "Address")
  String get address;
  @override
  @JsonKey(name: "addrHouseNo")
  String get addrHouseNo;
  @override // บ้านเลขที่
  @JsonKey(name: "addrVillageNo")
  String get addrVillageNo;
  @override // หมู่ที่
  @JsonKey(name: "addrLane")
  String get addrSoi;
  @override // ซอย
  @JsonKey(name: "addrRoad")
  String get addrRoad;
  @override // ถนน
  @JsonKey(name: "addrTambol")
  String get addrSubDistrict;
  @override // ตำบล
  @JsonKey(name: "addrAmphur")
  String get addrDistrict;
  @override // อำเภอ
  @JsonKey(name: "addrProvince")
  String get addrProvince;
  @override // จังหวัด
  @JsonKey(name: "Issuer")
  String get issuer;
  @override // ผู้ออกบัตร
  @JsonKey(name: "Issue")
  String get issueDate;
  @override // วันออกบัตร
  @JsonKey(name: "Issue_Expire")
  String get expireDate;
  @override // วันหมดอายุ
  @JsonKey(name: "PhotoRaw")
  String get photoBytes;
  @override
  @JsonKey(ignore: true)
  _$$CardInfoCSharpJsonImplCopyWith<_$CardInfoCSharpJsonImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$CardInfoResult {
  String get cid => throw _privateConstructorUsedError;
  set cid(String value) => throw _privateConstructorUsedError;
  String get birthDay => throw _privateConstructorUsedError;
  set birthDay(String value) => throw _privateConstructorUsedError;
  Gender? get gender => throw _privateConstructorUsedError;
  set gender(Gender? value) => throw _privateConstructorUsedError;
  String get thPrefix => throw _privateConstructorUsedError;
  set thPrefix(String value) => throw _privateConstructorUsedError;
  String get thFirstName => throw _privateConstructorUsedError;
  set thFirstName(String value) => throw _privateConstructorUsedError;
  String get thLastName => throw _privateConstructorUsedError;
  set thLastName(String value) => throw _privateConstructorUsedError;
  String get enPrefix => throw _privateConstructorUsedError;
  set enPrefix(String value) => throw _privateConstructorUsedError;
  String get enFirstName => throw _privateConstructorUsedError;
  set enFirstName(String value) => throw _privateConstructorUsedError;
  String get enLastName => throw _privateConstructorUsedError;
  set enLastName(String value) => throw _privateConstructorUsedError;
  String get address => throw _privateConstructorUsedError;
  set address(String value) => throw _privateConstructorUsedError;
  String get addrHouseNo => throw _privateConstructorUsedError;
  set addrHouseNo(String value) =>
      throw _privateConstructorUsedError; // บ้านเลขที่
  String get addrVillageNo => throw _privateConstructorUsedError; // บ้านเลขที่
  set addrVillageNo(String value) =>
      throw _privateConstructorUsedError; // หมู่ที่
  String get addrSoi => throw _privateConstructorUsedError; // หมู่ที่
  set addrSoi(String value) => throw _privateConstructorUsedError; // ซอย
  String get addrRoad => throw _privateConstructorUsedError; // ซอย
  set addrRoad(String value) => throw _privateConstructorUsedError; // ถนน
  String get addrSubDistrict => throw _privateConstructorUsedError; // ถนน
  set addrSubDistrict(String value) =>
      throw _privateConstructorUsedError; // ตำบล
  String get addrDistrict => throw _privateConstructorUsedError; // ตำบล
  set addrDistrict(String value) => throw _privateConstructorUsedError; // อำเภอ
  String get addrProvince => throw _privateConstructorUsedError; // อำเภอ
  set addrProvince(String value) =>
      throw _privateConstructorUsedError; // จังหวัด
  String get issueDate => throw _privateConstructorUsedError; // จังหวัด
  set issueDate(String value) =>
      throw _privateConstructorUsedError; // วันออกบัตร
  String get expireDate => throw _privateConstructorUsedError; // วันออกบัตร
  set expireDate(String value) =>
      throw _privateConstructorUsedError; // วันหมดอายุ
  Uint8List? get photoBytes => throw _privateConstructorUsedError; // วันหมดอายุ
  set photoBytes(Uint8List? value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CardInfoResultCopyWith<CardInfoResult> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CardInfoResultCopyWith<$Res> {
  factory $CardInfoResultCopyWith(
          CardInfoResult value, $Res Function(CardInfoResult) then) =
      _$CardInfoResultCopyWithImpl<$Res, CardInfoResult>;
  @useResult
  $Res call(
      {String cid,
      String birthDay,
      Gender? gender,
      String thPrefix,
      String thFirstName,
      String thLastName,
      String enPrefix,
      String enFirstName,
      String enLastName,
      String address,
      String addrHouseNo,
      String addrVillageNo,
      String addrSoi,
      String addrRoad,
      String addrSubDistrict,
      String addrDistrict,
      String addrProvince,
      String issueDate,
      String expireDate,
      Uint8List? photoBytes});
}

/// @nodoc
class _$CardInfoResultCopyWithImpl<$Res, $Val extends CardInfoResult>
    implements $CardInfoResultCopyWith<$Res> {
  _$CardInfoResultCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cid = null,
    Object? birthDay = null,
    Object? gender = freezed,
    Object? thPrefix = null,
    Object? thFirstName = null,
    Object? thLastName = null,
    Object? enPrefix = null,
    Object? enFirstName = null,
    Object? enLastName = null,
    Object? address = null,
    Object? addrHouseNo = null,
    Object? addrVillageNo = null,
    Object? addrSoi = null,
    Object? addrRoad = null,
    Object? addrSubDistrict = null,
    Object? addrDistrict = null,
    Object? addrProvince = null,
    Object? issueDate = null,
    Object? expireDate = null,
    Object? photoBytes = freezed,
  }) {
    return _then(_value.copyWith(
      cid: null == cid
          ? _value.cid
          : cid // ignore: cast_nullable_to_non_nullable
              as String,
      birthDay: null == birthDay
          ? _value.birthDay
          : birthDay // ignore: cast_nullable_to_non_nullable
              as String,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as Gender?,
      thPrefix: null == thPrefix
          ? _value.thPrefix
          : thPrefix // ignore: cast_nullable_to_non_nullable
              as String,
      thFirstName: null == thFirstName
          ? _value.thFirstName
          : thFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      thLastName: null == thLastName
          ? _value.thLastName
          : thLastName // ignore: cast_nullable_to_non_nullable
              as String,
      enPrefix: null == enPrefix
          ? _value.enPrefix
          : enPrefix // ignore: cast_nullable_to_non_nullable
              as String,
      enFirstName: null == enFirstName
          ? _value.enFirstName
          : enFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      enLastName: null == enLastName
          ? _value.enLastName
          : enLastName // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      addrHouseNo: null == addrHouseNo
          ? _value.addrHouseNo
          : addrHouseNo // ignore: cast_nullable_to_non_nullable
              as String,
      addrVillageNo: null == addrVillageNo
          ? _value.addrVillageNo
          : addrVillageNo // ignore: cast_nullable_to_non_nullable
              as String,
      addrSoi: null == addrSoi
          ? _value.addrSoi
          : addrSoi // ignore: cast_nullable_to_non_nullable
              as String,
      addrRoad: null == addrRoad
          ? _value.addrRoad
          : addrRoad // ignore: cast_nullable_to_non_nullable
              as String,
      addrSubDistrict: null == addrSubDistrict
          ? _value.addrSubDistrict
          : addrSubDistrict // ignore: cast_nullable_to_non_nullable
              as String,
      addrDistrict: null == addrDistrict
          ? _value.addrDistrict
          : addrDistrict // ignore: cast_nullable_to_non_nullable
              as String,
      addrProvince: null == addrProvince
          ? _value.addrProvince
          : addrProvince // ignore: cast_nullable_to_non_nullable
              as String,
      issueDate: null == issueDate
          ? _value.issueDate
          : issueDate // ignore: cast_nullable_to_non_nullable
              as String,
      expireDate: null == expireDate
          ? _value.expireDate
          : expireDate // ignore: cast_nullable_to_non_nullable
              as String,
      photoBytes: freezed == photoBytes
          ? _value.photoBytes
          : photoBytes // ignore: cast_nullable_to_non_nullable
              as Uint8List?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CardInfoResultImplCopyWith<$Res>
    implements $CardInfoResultCopyWith<$Res> {
  factory _$$CardInfoResultImplCopyWith(_$CardInfoResultImpl value,
          $Res Function(_$CardInfoResultImpl) then) =
      __$$CardInfoResultImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String cid,
      String birthDay,
      Gender? gender,
      String thPrefix,
      String thFirstName,
      String thLastName,
      String enPrefix,
      String enFirstName,
      String enLastName,
      String address,
      String addrHouseNo,
      String addrVillageNo,
      String addrSoi,
      String addrRoad,
      String addrSubDistrict,
      String addrDistrict,
      String addrProvince,
      String issueDate,
      String expireDate,
      Uint8List? photoBytes});
}

/// @nodoc
class __$$CardInfoResultImplCopyWithImpl<$Res>
    extends _$CardInfoResultCopyWithImpl<$Res, _$CardInfoResultImpl>
    implements _$$CardInfoResultImplCopyWith<$Res> {
  __$$CardInfoResultImplCopyWithImpl(
      _$CardInfoResultImpl _value, $Res Function(_$CardInfoResultImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cid = null,
    Object? birthDay = null,
    Object? gender = freezed,
    Object? thPrefix = null,
    Object? thFirstName = null,
    Object? thLastName = null,
    Object? enPrefix = null,
    Object? enFirstName = null,
    Object? enLastName = null,
    Object? address = null,
    Object? addrHouseNo = null,
    Object? addrVillageNo = null,
    Object? addrSoi = null,
    Object? addrRoad = null,
    Object? addrSubDistrict = null,
    Object? addrDistrict = null,
    Object? addrProvince = null,
    Object? issueDate = null,
    Object? expireDate = null,
    Object? photoBytes = freezed,
  }) {
    return _then(_$CardInfoResultImpl(
      cid: null == cid
          ? _value.cid
          : cid // ignore: cast_nullable_to_non_nullable
              as String,
      birthDay: null == birthDay
          ? _value.birthDay
          : birthDay // ignore: cast_nullable_to_non_nullable
              as String,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as Gender?,
      thPrefix: null == thPrefix
          ? _value.thPrefix
          : thPrefix // ignore: cast_nullable_to_non_nullable
              as String,
      thFirstName: null == thFirstName
          ? _value.thFirstName
          : thFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      thLastName: null == thLastName
          ? _value.thLastName
          : thLastName // ignore: cast_nullable_to_non_nullable
              as String,
      enPrefix: null == enPrefix
          ? _value.enPrefix
          : enPrefix // ignore: cast_nullable_to_non_nullable
              as String,
      enFirstName: null == enFirstName
          ? _value.enFirstName
          : enFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      enLastName: null == enLastName
          ? _value.enLastName
          : enLastName // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      addrHouseNo: null == addrHouseNo
          ? _value.addrHouseNo
          : addrHouseNo // ignore: cast_nullable_to_non_nullable
              as String,
      addrVillageNo: null == addrVillageNo
          ? _value.addrVillageNo
          : addrVillageNo // ignore: cast_nullable_to_non_nullable
              as String,
      addrSoi: null == addrSoi
          ? _value.addrSoi
          : addrSoi // ignore: cast_nullable_to_non_nullable
              as String,
      addrRoad: null == addrRoad
          ? _value.addrRoad
          : addrRoad // ignore: cast_nullable_to_non_nullable
              as String,
      addrSubDistrict: null == addrSubDistrict
          ? _value.addrSubDistrict
          : addrSubDistrict // ignore: cast_nullable_to_non_nullable
              as String,
      addrDistrict: null == addrDistrict
          ? _value.addrDistrict
          : addrDistrict // ignore: cast_nullable_to_non_nullable
              as String,
      addrProvince: null == addrProvince
          ? _value.addrProvince
          : addrProvince // ignore: cast_nullable_to_non_nullable
              as String,
      issueDate: null == issueDate
          ? _value.issueDate
          : issueDate // ignore: cast_nullable_to_non_nullable
              as String,
      expireDate: null == expireDate
          ? _value.expireDate
          : expireDate // ignore: cast_nullable_to_non_nullable
              as String,
      photoBytes: freezed == photoBytes
          ? _value.photoBytes
          : photoBytes // ignore: cast_nullable_to_non_nullable
              as Uint8List?,
    ));
  }
}

/// @nodoc

class _$CardInfoResultImpl extends _CardInfoResult {
  _$CardInfoResultImpl(
      {this.cid = "",
      this.birthDay = "",
      this.gender,
      this.thPrefix = "",
      this.thFirstName = "",
      this.thLastName = "",
      this.enPrefix = "",
      this.enFirstName = "",
      this.enLastName = "",
      this.address = "",
      this.addrHouseNo = "",
      this.addrVillageNo = "",
      this.addrSoi = "",
      this.addrRoad = "",
      this.addrSubDistrict = "",
      this.addrDistrict = "",
      this.addrProvince = "",
      this.issueDate = "",
      this.expireDate = "",
      this.photoBytes})
      : super._();

  @override
  @JsonKey()
  String cid;
  @override
  @JsonKey()
  String birthDay;
  @override
  Gender? gender;
  @override
  @JsonKey()
  String thPrefix;
  @override
  @JsonKey()
  String thFirstName;
  @override
  @JsonKey()
  String thLastName;
  @override
  @JsonKey()
  String enPrefix;
  @override
  @JsonKey()
  String enFirstName;
  @override
  @JsonKey()
  String enLastName;
  @override
  @JsonKey()
  String address;
  @override
  @JsonKey()
  String addrHouseNo;
// บ้านเลขที่
  @override
  @JsonKey()
  String addrVillageNo;
// หมู่ที่
  @override
  @JsonKey()
  String addrSoi;
// ซอย
  @override
  @JsonKey()
  String addrRoad;
// ถนน
  @override
  @JsonKey()
  String addrSubDistrict;
// ตำบล
  @override
  @JsonKey()
  String addrDistrict;
// อำเภอ
  @override
  @JsonKey()
  String addrProvince;
// จังหวัด
  @override
  @JsonKey()
  String issueDate;
// วันออกบัตร
  @override
  @JsonKey()
  String expireDate;
// วันหมดอายุ
  @override
  Uint8List? photoBytes;

  @override
  String toString() {
    return 'CardInfoResult(cid: $cid, birthDay: $birthDay, gender: $gender, thPrefix: $thPrefix, thFirstName: $thFirstName, thLastName: $thLastName, enPrefix: $enPrefix, enFirstName: $enFirstName, enLastName: $enLastName, address: $address, addrHouseNo: $addrHouseNo, addrVillageNo: $addrVillageNo, addrSoi: $addrSoi, addrRoad: $addrRoad, addrSubDistrict: $addrSubDistrict, addrDistrict: $addrDistrict, addrProvince: $addrProvince, issueDate: $issueDate, expireDate: $expireDate, photoBytes: $photoBytes)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CardInfoResultImplCopyWith<_$CardInfoResultImpl> get copyWith =>
      __$$CardInfoResultImplCopyWithImpl<_$CardInfoResultImpl>(
          this, _$identity);
}

abstract class _CardInfoResult extends CardInfoResult {
  factory _CardInfoResult(
      {String cid,
      String birthDay,
      Gender? gender,
      String thPrefix,
      String thFirstName,
      String thLastName,
      String enPrefix,
      String enFirstName,
      String enLastName,
      String address,
      String addrHouseNo,
      String addrVillageNo,
      String addrSoi,
      String addrRoad,
      String addrSubDistrict,
      String addrDistrict,
      String addrProvince,
      String issueDate,
      String expireDate,
      Uint8List? photoBytes}) = _$CardInfoResultImpl;
  _CardInfoResult._() : super._();

  @override
  String get cid;
  set cid(String value);
  @override
  String get birthDay;
  set birthDay(String value);
  @override
  Gender? get gender;
  set gender(Gender? value);
  @override
  String get thPrefix;
  set thPrefix(String value);
  @override
  String get thFirstName;
  set thFirstName(String value);
  @override
  String get thLastName;
  set thLastName(String value);
  @override
  String get enPrefix;
  set enPrefix(String value);
  @override
  String get enFirstName;
  set enFirstName(String value);
  @override
  String get enLastName;
  set enLastName(String value);
  @override
  String get address;
  set address(String value);
  @override
  String get addrHouseNo;
  set addrHouseNo(String value);
  @override // บ้านเลขที่
  String get addrVillageNo; // บ้านเลขที่
  set addrVillageNo(String value);
  @override // หมู่ที่
  String get addrSoi; // หมู่ที่
  set addrSoi(String value);
  @override // ซอย
  String get addrRoad; // ซอย
  set addrRoad(String value);
  @override // ถนน
  String get addrSubDistrict; // ถนน
  set addrSubDistrict(String value);
  @override // ตำบล
  String get addrDistrict; // ตำบล
  set addrDistrict(String value);
  @override // อำเภอ
  String get addrProvince; // อำเภอ
  set addrProvince(String value);
  @override // จังหวัด
  String get issueDate; // จังหวัด
  set issueDate(String value);
  @override // วันออกบัตร
  String get expireDate; // วันออกบัตร
  set expireDate(String value);
  @override // วันหมดอายุ
  Uint8List? get photoBytes; // วันหมดอายุ
  set photoBytes(Uint8List? value);
  @override
  @JsonKey(ignore: true)
  _$$CardInfoResultImplCopyWith<_$CardInfoResultImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
