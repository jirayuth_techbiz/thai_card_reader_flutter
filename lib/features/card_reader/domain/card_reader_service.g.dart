// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_reader_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$cardReaderServiceHash() => r'808e3c3856f2093049c372618cccc0aa09b33cc0';

/// See also [cardReaderService].
@ProviderFor(cardReaderService)
final cardReaderServiceProvider =
    AutoDisposeProvider<CardReaderService>.internal(
  cardReaderService,
  name: r'cardReaderServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$cardReaderServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CardReaderServiceRef = AutoDisposeProviderRef<CardReaderService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
