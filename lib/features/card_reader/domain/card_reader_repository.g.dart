// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_reader_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$cardReaderRepositoryHash() =>
    r'c735456d3e87ed1281f68b13d7f869b0535d02d7';

/// See also [cardReaderRepository].
@ProviderFor(cardReaderRepository)
final cardReaderRepositoryProvider =
    AutoDisposeProvider<CardReaderRepository>.internal(
  cardReaderRepository,
  name: r'cardReaderRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$cardReaderRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CardReaderRepositoryRef = AutoDisposeProviderRef<CardReaderRepository>;
String _$cardReaderSwiftRepositoryHash() =>
    r'1ab2417e796988df5f6160f470553fda338d23b6';

/// See also [cardReaderSwiftRepository].
@ProviderFor(cardReaderSwiftRepository)
final cardReaderSwiftRepositoryProvider =
    AutoDisposeProvider<CardReaderSwiftRepository>.internal(
  cardReaderSwiftRepository,
  name: r'cardReaderSwiftRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$cardReaderSwiftRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CardReaderSwiftRepositoryRef
    = AutoDisposeProviderRef<CardReaderSwiftRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
