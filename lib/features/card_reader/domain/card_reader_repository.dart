import 'package:flutter/foundation.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:thai_card_reader_flutter/features/card_reader/data/card_reader_repository_swift_impl.dart';
import 'package:thai_card_reader_flutter/features/card_reader/data/card_reader_repository_csharp_impl.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_info.dart';
import 'package:thai_card_reader_flutter/shared/app_l10n_repository.dart';
import 'package:thai_card_reader_flutter/shared/domain/card_reader_swift_pigeon_api.dart';
import 'package:thai_card_reader_flutter/shared/domain/csharp_json_rpc_api.dart';

part 'card_reader_repository.g.dart';

@riverpod
CardReaderRepository cardReaderRepository(CardReaderRepositoryRef ref) {

  switch (defaultTargetPlatform) {
    case TargetPlatform.windows:
    final cardReaderWrapper = ref.watch(cSharpJsonRpcProvider);
      return CardReaderRepositoryCSharpImpl(ref,
          cardReaderWrapper: cardReaderWrapper);
    case TargetPlatform.macOS:
    case TargetPlatform.iOS:
      final cardReaderSwiftAPI = ref.watch(cardReaderSwiftAPIProvider);
      final locale = ref.watch(localeObserverProvider);
      return CardReaderRepositorySwiftImpl(ref,
          cardReaderSwiftAPI: cardReaderSwiftAPI, locale: locale);
    default:
      throw UnsupportedError('Unsupported platform');
  }
}

@riverpod
CardReaderSwiftRepository cardReaderSwiftRepository(
    CardReaderRepositoryRef ref) {
  final cardReaderSwiftAPI = ref.watch(cardReaderSwiftAPIProvider);
  final locale = ref.watch(localeObserverProvider);
    return CardReaderRepositorySwiftImpl(ref,
        cardReaderSwiftAPI: cardReaderSwiftAPI, locale: locale);
}

abstract class CardReaderSwiftRepository extends CardReaderRepository {
  Stream<bool> isCardCurrentlyReading();
  Stream<int> getPercent();
  Future<bool> disconnect();
}

abstract class CardReaderRepository {
  Stream<bool> isCardInserted(String reader);
  Future<CardInfoCSharpJson> getCardInfo(String reader);
}
