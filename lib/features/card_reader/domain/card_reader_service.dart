import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:thai_card_reader_flutter/features/card_reader/application/card_reader_services_impl.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_info.dart';
import 'package:thai_card_reader_flutter/features/card_reader/domain/card_reader_repository.dart';

part 'card_reader_service.g.dart';

@riverpod
CardReaderService cardReaderService(CardReaderServiceRef ref) {
  final cardReaderRepository = ref.watch(cardReaderRepositoryProvider);
  return CardReaderServiceImpl(cardReaderRepository);
}

abstract class CardReaderService {
  Future<void> openUrl(CardInfoResult cardInfo);
  Future<CardInfoResult> getCardInfo(CardInfoCSharpJson cardInfoJson);
}
