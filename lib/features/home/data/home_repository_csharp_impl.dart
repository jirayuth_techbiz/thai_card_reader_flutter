import 'package:flutter/foundation.dart';
import 'package:thai_card_reader_flutter/features/home/domain/home_repository.dart';
import 'package:thai_card_reader_flutter/shared/API/thai_card_reader_rpc.dart';

class HomeRepositoryCSharpImpl implements HomeRepository {
  final CSharpJsonRpc cardReaderWrapper;

  HomeRepositoryCSharpImpl(this.cardReaderWrapper);

  @override
  Future<void> initialize() async {
    await cardReaderWrapper.startProcess();
  }

  @override
  Future<List<String>> getReadersList() async {
    try {
      final result = await cardReaderWrapper.invoke(methodName: 'GetReaders');
      if (result is List<String>) {
        return (result).map((e) => e.toString()).toList();
      } else if (result is Map) {
        if (result.containsKey("message")) {
          throw Exception('getReadersList: Error reading card - ${result["message"]}');
        }
        return [];
      } else {
        throw Exception('getReadersList: Error reading card - $result');
      }
    } catch (e) {
      debugPrint(e.toString());
      return [];
    }
  }

  @override
  Future<String> getVersion() async {
    final result = await cardReaderWrapper.invoke(methodName: 'GetVersion');
    return result;
  }

  @override
  Future<bool> connect(String reader) async {
    final result =
        await cardReaderWrapper.invoke(methodName: 'getReader', param: reader);
    return result;
  }

  ///For Testing only!
  @override
  Future<String> pingTest() async {
    final result =
        await cardReaderWrapper.invoke(methodName: 'Ping', params: ["Test"]);
    return result;
  }
}
