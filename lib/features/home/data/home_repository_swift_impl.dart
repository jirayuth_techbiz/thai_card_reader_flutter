import 'package:flutter/foundation.dart';
import 'package:thai_card_reader_flutter/const/error_swift_enum.dart';
import 'package:thai_card_reader_flutter/features/home/domain/home_repository.dart';
import 'package:thai_card_reader_flutter/generated/app_localizations.dart';
import 'package:thai_card_reader_flutter/generated/swift_pigeon.g.dart';

class HomeRepositorySwiftImpl implements HomeRepository {
  final CardReaderSwiftAPI cardReaderSwiftAPI;
  final AppLocalizations locale;

  HomeRepositorySwiftImpl(this.cardReaderSwiftAPI, this.locale);

  @override
  Future<bool> connect(
    String reader,
  ) async {
    final res =
        await cardReaderSwiftAPI.connect(ConnectRequest(readerName: reader));
    try {
      if (res.errorCode != null) {
        debugPrint(res.debugLog.toString());
        throw Exception(CardReaderSwiftError.getErrorType(res.errorCode!)
            .getErrorDescription(locale));
      }

      final bool result = res.result ?? false;
      return result;
    } catch (e) {
      debugPrint(e.toString());
      throw Exception(CardReaderSwiftError.getErrorType(res.errorCode!).getErrorDescription(locale));
    }
  }

  @override
  Future<List<String>> getReadersList() async {
    GetReadersResponse res;
    res = await cardReaderSwiftAPI.getScanReaders();

    try {
      List<String> readers = [];
      if (res.argData is String) {
        final String reader = res.argData as String;
        readers.add(reader);
      } else if (res.argData is List<Object?>) {
        final List<Object?> readerList = res.argData as List<Object?>;
        for (final reader in readerList) {
          readers.add(reader as String);
        }
      } else {
         throw Exception(CardReaderSwiftError.getErrorType(res.errorCode!)
            .getErrorDescription(locale));
      }
      return readers;
    } catch (e) {
      debugPrint(e.toString());
       throw Exception(CardReaderSwiftError.getErrorType(res.errorCode!)
            .getErrorDescription(locale));
    }
  }

  @override
  Future<String> getVersion() {
    // TODO: implement getVersion
    throw UnimplementedError();
  }

  @override
  Future<void> initialize() async {
    try {
      await cardReaderSwiftAPI.initializeCardReader();
      debugPrint('initialize');
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  @override
  Future<String> pingTest() {
    // TODO: implement pingTest
    throw UnimplementedError();
  }
}
