import 'package:flutter/foundation.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:thai_card_reader_flutter/features/home/data/home_repository_swift_impl.dart';
import 'package:thai_card_reader_flutter/features/home/data/home_repository_csharp_impl.dart';
import 'package:thai_card_reader_flutter/generated/swift_pigeon.g.dart';
import 'package:thai_card_reader_flutter/shared/app_l10n_repository.dart';
import 'package:thai_card_reader_flutter/shared/domain/card_reader_swift_pigeon_api.dart';
import 'package:thai_card_reader_flutter/shared/domain/csharp_json_rpc_api.dart';

part 'home_repository.g.dart';

@riverpod
HomeRepository homeRepository(HomeRepositoryRef ref) {
  switch (defaultTargetPlatform) {
    case TargetPlatform.windows:
      final cardReaderWrapper = ref.watch(cSharpJsonRpcProvider);
      return HomeRepositoryCSharpImpl(cardReaderWrapper);
    case TargetPlatform.macOS:
    case TargetPlatform.iOS:
      final cardReaderSwiftAPI = ref.watch(cardReaderSwiftAPIProvider);
      final locale = ref.watch(localeObserverProvider);
      return HomeRepositorySwiftImpl(cardReaderSwiftAPI, locale);
    default:
      throw UnsupportedError('Unsupported platform');
  }
}

abstract class HomeSwiftRepository {
  Future<InitializeResponse> initialize();
  Future<GetReadersResponse> getReadersList();
  Future<ConnectResponse> connect(String reader);
}

abstract class HomeRepository {
  Future<void> initialize();
  Future<List<String>> getReadersList();
  Future<String> getVersion();
  Future<bool> connect(String reader);
  Future<String> pingTest();
}
