import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:thai_card_reader_flutter/const/error_swift_enum.dart';
import 'package:thai_card_reader_flutter/features/home/presentation/state/home_picker_state.dart';
import 'package:thai_card_reader_flutter/generated/assets.gen.dart';
import 'package:thai_card_reader_flutter/shared/app_l10n_repository.dart';
import 'package:thai_card_reader_flutter/shared/async_value_widget.dart';
import 'package:thai_card_reader_flutter/shared/background_view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeView extends ConsumerStatefulWidget {
  const HomeView({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomeViewState();
}

class _HomeViewState extends ConsumerState<HomeView> {
  late bool isFirstLaunch;

  @override
  void initState() {
    super.initState();
    isFirstLaunch = true;
  }

  @override
  Widget build(BuildContext context) {
    final itemList = ref.watch(homePickerListStateProvider);
    final selectedItem = ref.watch(homePickerSelectionStateProvider);
    final selectedText = ref.watch(homePickerTextSelectionStateProvider);
    final homeRepositoryState = ref.watch(homeRepositoryStateProvider);
    final translation = context.localization;

    void showReadersList(
      Widget child,
    ) {
      showCupertinoModalPopup<void>(
        context: context,
        builder: (context) {
          return Container(
            height: 216.h,
            padding: const EdgeInsets.only(top: 6.0),
            margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            color: CupertinoColors.systemBackground.resolveFrom(context),
            child: SafeArea(
              top: false,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text(
                          translation.cancelTxt,
                          style: TextStyle(
                            color:
                                CupertinoColors.systemBlue.resolveFrom(context),
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          translation.okTxt,
                          style: TextStyle(
                            color:
                                CupertinoColors.systemBlue.resolveFrom(context),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const Divider(),
                  Expanded(child: child),
                ],
              ),
            ),
          );
        },
      );
    }

    return ICSBackgroundView(
      child: AsyncValueWidget(
        value: homeRepositoryState,
        error: (e, st) {
          return GeneralPopUpError(
              title: translation.errorTxt, content: e.toString());
        },
        data: (api) => SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const _ICSAppNameText(),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20.0.w),
                child: SizedBox(
                  width: 130.r,
                  height: 130.r,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(35.0.r),
                    child: Image.asset(
                      fit: BoxFit.fill,
                      Assets.icon.appstore.keyName,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width.r / 2,
                height: 100.h,
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.all(8.r),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: CupertinoColors.black, width: 1),
                          borderRadius: BorderRadius.circular(8.r),
                        ),
                        child: AsyncValueWidget(
                          value: itemList,
                          loading: () =>
                              _ReaderPicker(selectedText: selectedText),
                          data: (result) => GestureDetector(
                            onTap: () async {
                              ref
                                  .read(homePickerListStateProvider.notifier)
                                  .fetchReaderList()
                                  .then(
                                (res) {
                                  if (res.isEmpty) {
                                    _showErrorDialog(
                                      translation.errorTxt,
                                      translation.homeErrorNoDevice,
                                    );
                                  } else {
                                    ref
                                        .read(
                                            homePickerTextSelectionStateProvider
                                                .notifier)
                                        .updateSelectorText(res.first);
                                    showReadersList(
                                      _CardReaderCupertinoPicker(
                                          selectedItem: selectedItem,
                                          ref: ref,
                                          selectedItemList: res),
                                    );
                                  }
                                },
                              );
                            },
                            child: _ReaderPicker(selectedText: selectedText),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0),
                      child: IconButton(
                        onPressed: () {
                          ref.invalidate(homePickerTextSelectionStateProvider);
                          return ref.refresh(homePickerListStateProvider);
                        },
                        icon: const Icon(CupertinoIcons.arrow_2_circlepath),
                      ),
                    )
                  ],
                ),
              ),
              AsyncValueWidget(
                value: itemList,
                loading: () => const CupertinoActivityIndicator(),
                error: (e, st) => CupertinoButton(
                  color: CupertinoColors.systemGrey,
                  child: Text(translation.homeConnect),
                  onPressed: () {},
                ),
                data: (data) => Padding(
                  padding: EdgeInsets.symmetric(vertical: 16.0.h),
                  child: Visibility(
                    visible: (data.isNotEmpty),
                    replacement: CupertinoButton(
                      color: CupertinoColors.systemGrey,
                      child: Text(translation.homeConnect),
                      onPressed: () {},
                    ),
                    child: CupertinoButton.filled(
                        child: Text(translation.homeConnect),
                        onPressed: () async {
                          try {
                            bool result = await api.connect(selectedText ?? "");
                            if (result && context.mounted) {
                              Navigator.of(context).pushNamed('/card-reader',
                                  arguments: selectedText);
                            } else {
                              _showErrorDialog(
                                translation.errorTxt,
                                translation.homeErrorConnect,
                              );
                            }
                          } catch (e) {
                            _showErrorDialog(
                              translation.errorTxt,
                              e.toString(),
                            );
                          }
                        }),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showErrorDialog(String title, String content) async {
    final translation = context.localization;

    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(title),
          content: Text(content),
          actions: [
            CupertinoDialogAction(
              child: Text(translation.okTxt),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class _ReaderPicker extends StatelessWidget {
  const _ReaderPicker({
    this.selectedText,
  });

  final String? selectedText;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            selectedText ?? "Choose your device",
            textAlign: TextAlign.start,
          ),
        ),
        const Icon(
          CupertinoIcons.chevron_down,
          textDirection: TextDirection.ltr,
        )
      ],
    );
  }
}

class _CardReaderCupertinoPicker extends StatelessWidget {
  const _CardReaderCupertinoPicker({
    super.key,
    required this.selectedItem,
    required this.ref,
    required this.selectedItemList,
  });

  final int selectedItem;
  final WidgetRef ref;
  final List<String> selectedItemList;

  @override
  Widget build(BuildContext context) {
    return CupertinoPicker(
      selectionOverlay: CupertinoPickerDefaultSelectionOverlay(
        background: CupertinoColors.black.withOpacity(0.12),
      ),
      useMagnifier: true,
      magnification: 1.5,
      itemExtent: 32,
      scrollController: FixedExtentScrollController(initialItem: selectedItem),
      onSelectedItemChanged: (int selectedItemNumber) {
        ref
            .read(homePickerSelectionStateProvider.notifier)
            .updateSelector(selectedItemNumber);
      },
      children: List.generate(selectedItemList.length, (int index) {
        return Center(
          child: Text(
            selectedItemList[index],
            style: TextStyle(
              fontSize: 16.spMin,
            ),
          ),
        );
      }),
    );
  }
}

class _ICSAppNameText extends StatelessWidget {
  const _ICSAppNameText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final translation = context.localization;

    return Container(
      padding: EdgeInsets.only(
        left: 40.w,
        top: 40.h,
        bottom: 30.h,
      ),
      alignment: Alignment.centerLeft,
      child: Opacity(
        opacity: 0.5,
        child: Text(
          translation.homeTitle,
          style: TextStyle(fontSize: 20.spMin, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
