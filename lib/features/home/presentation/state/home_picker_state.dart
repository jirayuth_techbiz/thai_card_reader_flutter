import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:thai_card_reader_flutter/features/home/domain/home_repository.dart';

part 'home_picker_state.g.dart';

@riverpod
class HomePickerTextSelectionState extends _$HomePickerTextSelectionState {
  @override
  String? build() {
    return null;
  }

  updateSelectorText(String newValue) {
    state = newValue;
  }
}

@riverpod
class HomePickerSelectionState extends _$HomePickerSelectionState {
  @override
  int build() {
    return 0;
  }

  updateSelector(int selected) {
    state = selected;
  }
}

@riverpod
class HomeRepositoryState extends _$HomeRepositoryState {
  Future<HomeRepository> _initialize() async {
    final homeRepository = ref.watch(homeRepositoryProvider);
    await homeRepository.initialize();
    return homeRepository;
  }

  @override
  FutureOr<HomeRepository> build() async {
    return _initialize();
  }
}

@riverpod
class HomePickerListState extends _$HomePickerListState {
  @override
  FutureOr<List<String>> build() async {
    return [];
  }

  Future<List<String>> fetchReaderList() async {
    List<String> res = <String>[];
    state = const AsyncLoading();
    final homeRepository = ref.watch(homeRepositoryProvider);
    state = await AsyncValue.guard(() => homeRepository.getReadersList());
    res = state.value ?? <String>[];
    return res;
  }
}
