// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_picker_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$homePickerTextSelectionStateHash() =>
    r'29004e777fe7243af1a66985430321397cef1ffe';

/// See also [HomePickerTextSelectionState].
@ProviderFor(HomePickerTextSelectionState)
final homePickerTextSelectionStateProvider =
    AutoDisposeNotifierProvider<HomePickerTextSelectionState, String?>.internal(
  HomePickerTextSelectionState.new,
  name: r'homePickerTextSelectionStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$homePickerTextSelectionStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$HomePickerTextSelectionState = AutoDisposeNotifier<String?>;
String _$homePickerSelectionStateHash() =>
    r'fe3aca40a578ae6a39b4b8c347dbe4eb13e091ae';

/// See also [HomePickerSelectionState].
@ProviderFor(HomePickerSelectionState)
final homePickerSelectionStateProvider =
    AutoDisposeNotifierProvider<HomePickerSelectionState, int>.internal(
  HomePickerSelectionState.new,
  name: r'homePickerSelectionStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$homePickerSelectionStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$HomePickerSelectionState = AutoDisposeNotifier<int>;
String _$homeRepositoryStateHash() =>
    r'6a1aaa98c5855baf5cf66ec1b6a1199a44e42d69';

/// See also [HomeRepositoryState].
@ProviderFor(HomeRepositoryState)
final homeRepositoryStateProvider = AutoDisposeAsyncNotifierProvider<
    HomeRepositoryState, HomeRepository>.internal(
  HomeRepositoryState.new,
  name: r'homeRepositoryStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$homeRepositoryStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$HomeRepositoryState = AutoDisposeAsyncNotifier<HomeRepository>;
String _$homePickerListStateHash() =>
    r'bccedfc131f5b4b76a92c27381fc6ac292ae7b05';

/// See also [HomePickerListState].
@ProviderFor(HomePickerListState)
final homePickerListStateProvider = AutoDisposeAsyncNotifierProvider<
    HomePickerListState, List<String>>.internal(
  HomePickerListState.new,
  name: r'homePickerListStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$homePickerListStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$HomePickerListState = AutoDisposeAsyncNotifier<List<String>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
