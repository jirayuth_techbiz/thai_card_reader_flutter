import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'language_state.g.dart';

@Riverpod(keepAlive: true)
class LanguageState extends _$LanguageState {
  final String _currentLang = 'th';

  @override
  String build() {
    return _currentLang;
  }

  void setLang(String lang) {
    state = lang;
  }
}