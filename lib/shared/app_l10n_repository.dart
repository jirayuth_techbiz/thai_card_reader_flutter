import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:thai_card_reader_flutter/generated/app_localizations.dart';

part 'app_l10n_repository.g.dart';

@riverpod
AppLocalizations localeObserver(LocaleObserverRef ref) {
  ref.state = lookupAppLocalizations(ui.PlatformDispatcher.instance.locale);
  final observer = LocalObserver((locales) {
    ref.state = lookupAppLocalizations(ui.PlatformDispatcher.instance.locale);
  });

  final binding = WidgetsBinding.instance;
  binding.addObserver(observer);

  ref.onDispose(() {
    binding.removeObserver(observer);
  });

  return ref.state;
}

class LocalObserver extends WidgetsBindingObserver {
  LocalObserver(this._didChangeLocales);
  final void Function(List<Locale>? locales) _didChangeLocales;

  @override
  void didChangeLocales(List<Locale>? locales) {
    _didChangeLocales(locales);
  }
}

extension AppLocaleExtensions on BuildContext {
  AppLocalizations get localization => AppLocalizations.of(this)!;
}

class LocalizationHelper {
  static Future<AppLocalizations> load(Locale locale) async {
    const localizations = AppLocalizations.delegate;
    if (localizations.isSupported(locale)) {
      return await localizations.load(locale);
    }
    throw Exception("Locale not supported");
  }
}
