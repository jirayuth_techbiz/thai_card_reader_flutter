import 'package:intl/intl.dart';

extension TryParseDateTime on String {
  DateTime? tryParseDateTime() {
    List<String> datetimePattern = [
      "yyyy-MM-dd",
      "yyyyMMdd",
      "yyyy/MM/dd",
    ];

    for (var pattern in datetimePattern) {
      try {
        String newInput = this;
        DateTime newDate;
        if (pattern == "yyyyMMdd") {
          newInput = "${substring(0, 4)}-${substring(4, 6)}-${substring(6, 8)}";
          newDate = DateFormat("yyyy-MM-dd").parse(newInput);
        } else {
          newDate = DateFormat(pattern).parse(newInput);
        }

        return newDate;
      } catch (e) {
        continue;
      }
    }
    return null;
  }
}
