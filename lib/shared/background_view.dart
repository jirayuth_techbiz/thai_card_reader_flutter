import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:thai_card_reader_flutter/generated/assets.gen.dart';

class ICSBackgroundView extends ConsumerWidget {
  const ICSBackgroundView({
    super.key,
    required this.child,
  });

  final Widget? child;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return CupertinoPageScaffold(
      backgroundColor: CupertinoColors.systemGrey6,
      child: Container(
        width: MediaQuery.of(context).size.width.w,
        constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.height,
        ),
        decoration: BoxDecoration(
            color: CupertinoColors.inactiveGray,
            image: DecorationImage(
              image: AssetImage(Assets.bg.bg.path),
              fit: BoxFit.fill,
            ),
          ),
          child: child,
      ),
    );
  }
}
