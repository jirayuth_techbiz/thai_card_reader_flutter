import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:thai_card_reader_flutter/generated/swift_pigeon.g.dart';

part 'card_reader_swift_pigeon_api.g.dart';

/// Generate state from pigeon api
@Riverpod(keepAlive: true)
CardReaderSwiftAPI cardReaderSwiftAPI(CardReaderSwiftAPIRef ref) {
  return CardReaderSwiftAPI();
}