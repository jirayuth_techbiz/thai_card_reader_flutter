import 'package:flutter/foundation.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:thai_card_reader_flutter/shared/API/thai_card_reader_rpc.dart';

part 'csharp_json_rpc_api.g.dart';

@Riverpod(keepAlive: true)
CSharpJsonRpc cSharpJsonRpc(CSharpJsonRpcRef ref) {
  const windowsExecPath =
      "./vendor/ThaiNationalIDJsonRpc/bin/Release/win-x64/publish/ThaiNationalIDJsonRpc.exe";
  /// No AOT support for macOS
  // const macExecPath =
  //     "./vendor/ThaiNationalIDJsonRpc/bin/Release/net8.0/osx-x64/ThaiNationalIDJsonRpc";

  switch (defaultTargetPlatform) {
    case TargetPlatform.windows:
      return CSharpJsonRpc(windowsExecPath);
    /// No AOT support for macOS
    // case TargetPlatform.macOS: 
    //   return CSharpJsonRpc(macExecPath);
    default:
      throw UnsupportedError('Unsupported platform');
  }
}
