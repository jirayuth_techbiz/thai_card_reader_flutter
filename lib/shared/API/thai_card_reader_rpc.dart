///https://dart.dev/interop/c-interop
///
library;

import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

class CSharpJsonRpc {
  Process? cSharpProcess;
  final String _execPath;
  final Uuid _uuid = const Uuid();
  final List<CSharpRPCRequest> _requests = [];

  CSharpJsonRpc(this._execPath);

  // https://medium.com/@flightlesscoder/flutter-c-and-json-rpc-f325be6764bd
  // https://github.com/YehudaKremer/FlutterCsharpRpc/blob/main/dart/lib/src/csharp_rpc.dart
  Future<CSharpJsonRpc> startProcess() async {
    if (defaultTargetPlatform == TargetPlatform.macOS) await ensurePcscLiteInstalledOnMac();

    if (defaultTargetPlatform == TargetPlatform.windows ||
        defaultTargetPlatform == TargetPlatform.macOS) {
      cSharpProcess = await Process.start(
        _execPath,
        [],
      );
    } else {
      throw UnsupportedError('Unsupported platform');
    }
    cSharpProcess?.stdout.listen(_onDataReceived);
    cSharpProcess?.stderr.listen(_onLogReceived);
    return this;
  }

  Future<TResult> invoke<TResult>(
      {required String methodName, List<dynamic>? params, Object? param}) {
    var id = _uuid.v4();

    var jsonEncodedBody = jsonEncode({
      "jsonrpc": "2.0",
      "method": methodName,
      if (params != null || param != null) "params": params ?? [param],
      "id": id,
    });

    var contentLengthHeader = 'Content-Length: ${jsonEncodedBody.length}';
    var payloadMsg = "$contentLengthHeader\r\n\r\n$jsonEncodedBody";

    cSharpProcess?.stdin.write(payloadMsg);

    var csharpRpcRequest = CSharpRPCRequest<TResult>(id);
    _requests.add(csharpRpcRequest);

    return csharpRpcRequest.completer.future;
  }

  ///Parse the incoming data from the C# process
  Future<void> _onDataReceived(event) async {
    try {
      dynamic response = await compute(_parseResponse, event);
      var request = _requests.firstWhere(
        (r) => r.requestId == response["id"],
      );
      var result = response['result'];
      var err = response['error'];
      if (err != null) {
        request.completer.complete(err);
        _requests.remove(request);
      } else if (response.toString().isEmpty) {
        request.completer
            .complete("Empty Response : If you see this, contact developer");
        _requests.remove(request);
      } else {
        request.completer.complete(result);
        _requests.remove(request);
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  void _onLogReceived(List<int> event) {
    assert(() {
      debugPrint(utf8.decode(event));
      return true;
    }());
  }

  void dispose() {
    cSharpProcess?.kill();
    _requests.clear();
  }

  static dynamic _parseResponse(response) {
    var strMsg = utf8.decode(response);
    var parts = strMsg.split(RegExp(r'\r?\n\r?\n'));
    if (parts.length < 2) {
      throw const FormatException("Invalid message format");
    }
    var jsonStr = parts.last;
    return jsonDecode(jsonStr);
  }

  Future<void> ensurePcscLiteInstalledOnMac() async {
    var homeDir = Platform.environment['HOME'] ?? '';
    await Process.run('ln', ['-s', ' $homeDir/Documents/homebrew/opt/pcsc-lite/lib/libpcsclite.dylib', '$homeDir/Documents/homebrew/opt/pcsc-lite/lib/libpcsclite.so.1'], runInShell: true,);
    bool pcscExist = await _checkLibraryExists();
    if (!pcscExist) {
      await _installPcscLite();
      throw Exception("Download PCSC Lite complete, Please restart the app.");
    } else {
      debugPrint('PCSC Lite already installed.');
    }
  }

  Future<bool> _checkLibraryExists() async {
    // This command checks if the PCSC Lite library is installed
    var result = await Process.run('brew', ['list', 'pcsc-lite']);
    return result.stdout.toString().contains('pcsc-lite');
  }

  Future<void> _installPcscLite() async {
    // This command installs the PCSC Lite library
    await Process.run('brew', ['install', 'pcsc-lite']);
    
    debugPrint('PCSC Lite installed successfully.');
  }
}

class CSharpRPCRequest<TResult> {
  final String _requestId;
  String get requestId => this._requestId;

  final Completer<TResult> _completer = Completer();
  Completer<TResult> get completer => this._completer;

  CSharpRPCRequest(this._requestId);
}
