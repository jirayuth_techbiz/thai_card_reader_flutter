import 'package:pigeon/pigeon.dart';

@ConfigurePigeon(PigeonOptions(
  dartOut: 'lib/generated/swift_pigeon.g.dart',
  dartOptions: DartOptions(),
  swiftOut: 'ios/Runner/AppDelegate.g.swift',
  swiftOptions: SwiftOptions(),
  dartPackageName: 'com.techbusiness.icare.cardreader',
))

///https://blog.codemagic.io/working-with-native-elements/

class InitializeResponse implements BaseErrorResponse {
  String? result;

  @override
  Object? debugLog;

  @override
  int? errorCode;
}

class GetReadersResponse implements BaseErrorResponse {
  String? notifyId;
  String? messageType;
  String? caller;
  int? perc;
  Object? argData;

  @override
  Object? debugLog;

  @override
  int? errorCode;
}

class ConnectRequest {
  String? readerName;
}

class ConnectResponse implements BaseErrorResponse {
  bool? result;

  @override
  Object? debugLog;

  @override
  int? errorCode;
}

class GetCardInfoRequest {
  String? readerName;
}

class GetCardInfoResponse implements BaseErrorResponse {
  String? notifyId;
  String? messageType;
  String? caller;
  int? perc;
  Object? argData;

  @override
  Object? debugLog;

  @override
  int? errorCode;
}

class DisconnectResponse implements BaseErrorResponse {
  bool? result;

  @override
  Object? debugLog;

  @override
  int? errorCode;
}

@HostApi()
abstract class CardReaderSwiftAPI {
  ///เริ่มระบบ Card Reader หลังบ้าน
  InitializeResponse initializeCardReader();
  ///สแกนอ่านข้อมูลเครื่องอ่านบัตรทั้งหมดในละแวก
  GetReadersResponse getScanReaders();
  ///เชื่อมต่อเครื่องอ่านบัตรที่เลือก
  ConnectResponse connect(ConnectRequest request);
  /// ตัดการเชื่อมต่อเครื่องอ่านบัตร
  DisconnectResponse disconnect();
  ///อ่านข้อมูลบัตรที่เป็น Text-Based
  GetCardInfoResponse readCardTextInfo(GetCardInfoRequest request);
  ///อ่านข้อมูลรูปบัตร รูปบัตรจะอยู่ในรูปแบบ [Base64] ต้อง decode ก่อนใช้งาน
  GetCardInfoResponse readCardImage(GetCardInfoRequest request);
}

/// Error must be int!
///
/// เราจะ handle error ที่ฝั่ง Dart แทน (จะได้ใช้ได้ 2 ภาษา)
sealed class BaseErrorResponse {
  /// รหัส Error เอาไว้ผูกกับ [CardReaderSwiftError]
  int? errorCode;
  /// เอาไว้อ่านตอน Debug
  Object? debugLog;
}
