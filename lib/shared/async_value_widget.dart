import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

///Common Widget สำหรับแสดงผลข้อมูลจาก [AsyncValue]
class AsyncValueWidget<T> extends ConsumerWidget {
  const AsyncValueWidget({super.key, required this.value, required this.data, this.loading, this.error});
  
  /// ข้อมูลจาก [AsyncValue]
  final AsyncValue<T> value;

  /// เมื่อข้อมูลเป็น [AsyncValue.data] จะทำยังไงกับ Data ที่ได้มา
  final Widget Function(T) data;

  /// - Optional 
  /// 
  /// แสดง Widget ในระหว่างโหลดข้อมูล
  /// 
  /// ถ้าไม่มีจะแสดง [CircularProgressIndicator]
  final Widget Function()? loading;

  /// - Optional
  /// 
  /// แสดง Widget กรณีเกิด Error
  /// 
  /// ถ้าไม่มีจะแสดง [GeneralPopUpError]
  final Widget Function(Object e, StackTrace st)? error;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return value.when(
      data: data,
      error: error ?? (e, st) => Center(
          child: GeneralPopUpError(
        title: 'Error',
        content: kDebugMode ? "${e.toString()} \n " : e.toString(),
      )),
      loading: loading ?? () => const Align(
          alignment: Alignment.center,
          child: CircularProgressIndicator.adaptive()),
    );
  }
}

///Common Widget สำหรับแสดงผลข้อมูลจาก 
///
///- [AsyncValue.error]
///- [AppException]
class GeneralPopUpError extends ConsumerWidget {
  
  ///ชื่อของ Popup
  final String title;

  ///เนื้อหา Error ของ Popup
  final String content;

  const GeneralPopUpError(
      {super.key, required this.title, required this.content});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        TextButton(
          onPressed: () {
            try {
              Navigator.of(context).pop();
            } catch (e) {
              Navigator.of(context).pushNamed('/');
            }
          },
          child: const Text('Confirm'),
        ),
      ],
    );
  }
}