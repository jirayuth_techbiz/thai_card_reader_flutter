import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:thai_card_reader_flutter/features/card_reader/presentation/card_reader_view.dart';
import 'package:thai_card_reader_flutter/features/home/presentation/home_view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:thai_card_reader_flutter/generated/app_localizations.dart';
import 'package:thai_card_reader_flutter/shared/state/language_state.dart';

void main() {
  Future<void> initializingApp() async {
    WidgetsFlutterBinding.ensureInitialized();
  }

  runZonedGuarded<Future<void>>(() async {
    await initializingApp();

    return runApp(const ProviderScope(child: MainApp()));
  }, (err, stk) {});
}

class MainApp extends ConsumerWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final languageState = ref.watch(languageStateProvider);

    return ScreenUtilInit(
      minTextAdapt: true,
      builder: (_, child) => CupertinoApp(
        routes: {
          '/card-reader': (context) => const CardReaderView(),
        },
        home: child,
        locale: Locale(languageState),
        localizationsDelegates: const [
          AppLocalizations.delegate,
        ],
      ),
      child: const HomeView(),
    );
  }
}
