enum ScreenType implements Comparable<ScreenType> {
  desktop(width: 900),
  tablet(width: 600),
  mobile(width: 400);

  const ScreenType({required this.width});

  final int width;

  @override
  int compareTo(ScreenType other) => width - other.width;
}

///Not sure how to use
class FormFactor {
  ScreenType getScreenType(double width) {
    if (width >= ScreenType.desktop.width) {
      return ScreenType.desktop;
    } else if (width >= ScreenType.tablet.width) {
      return ScreenType.tablet;
    } else {
      return ScreenType.mobile;
    }
  }
}

enum CardReaderEnumSwift implements Comparable<CardReaderEnumSwift> {
  ///ใน Documentation ใช้ 1-Based Inde แต่ใน Code เราจะใช้ 0-Based Index
  /// 1: รหัสประจำตัวประชาชน
  cid(code: 0),
  /// 2: คำนำหน้าชื่อ
  thPrefix(code: 1),
  /// 3: ชื่อไทย
  thFirstName(code: 2),
  /// 4: นามสกุลไทย
  thMiddleName(code: 3),
  /// 5: นามสกุลไทย
  thLastName(code: 4),
  /// 6: คำนำหน้าชื่อ
  enPrefix(code: 5),
  /// 7: ชื่ออังกฤษ
  enFirstName(code: 6),
  /// 8: นามสกุลอังกฤษ
  enMiddleName(code: 7),
  /// 9: นามสกุลอังกฤษ
  enLastName(code: 8),
  /// 10: บ้านเลขที่
  addrHouseNo(code: 9),
  /// 11: หมู่ที่
  addrVillageNo(code: 10),
  /// 12: ตรอก/หมู่บ้าน
  addrVillageName(code: 11),
  /// 13: ซอย
  addrSoi(code: 12),
  /// 14: ถนน
  addrRoad(code: 13),
  /// 15: ตำบล
  addrSubDistrict(code: 14),
  /// 16: อำเภอ
  addrDistrict(code: 15),
  /// 17: จังหวัด
  addrProvince(code: 16),
  /// 18: เพศ
  gender(code: 17),
  /// 19: วันเกิด
  birthDay(code: 18),
  /// 20: ผู้ออกบัตร
  issuer(code: 19),
  /// 21: วันออกบัตร
  issueDate(code: 20),
  /// 22: วันหมดอายุ
  expireDate(code: 21),
  /// 23: เลขหมายคำขอ
  symbolCode(code: 22);

  const CardReaderEnumSwift({required this.code});

  final int code;

  @override 
  int compareTo(CardReaderEnumSwift other) => code - other.code;
}
