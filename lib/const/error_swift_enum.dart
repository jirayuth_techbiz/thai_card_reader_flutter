
import 'package:thai_card_reader_flutter/generated/app_localizations.dart';

enum CardReaderSwiftError implements Comparable<CardReaderSwiftError> {
  success(code: 0),
  internalError(code: -1),
  invalidLicense(code: -2),
  readerNotFound(code: -3),
  connectionError(code: -4),
  getPhotoError(code: -5),
  getTextError(code: -6),
  invalidCard(code: -7),
  unknownCardVersion(code: -8),
  disconnectionError(code: -9),
  initError(code: -10),
  readerNotSupported(code: -11),
  licenseFileError(code: -12),
  parameterError(code: -13),
  internetError(code: -15),
  cardNotFound(code: -16),
  licenseUpdateError(code: -18),
  licenseUpdatedPleaseRestart(code: -19);

  @override
  compareTo(CardReaderSwiftError other) => code - other.code;

  final int code;
  const CardReaderSwiftError({required this.code});

  factory CardReaderSwiftError.getErrorType(int code) {
    switch (code) {
      case 0:
        return CardReaderSwiftError.success;
      case -1:
        return CardReaderSwiftError.internalError;
      case -2:
        return CardReaderSwiftError.invalidLicense;
      case -3:
        return CardReaderSwiftError.readerNotFound;
      case -4:
        return CardReaderSwiftError.connectionError;
      case -5:
        return CardReaderSwiftError.getPhotoError;
      case -6:
        return CardReaderSwiftError.getTextError;
      case -7:
        return CardReaderSwiftError.invalidCard;
      case -8:
        return CardReaderSwiftError.unknownCardVersion;
      case -9:
        return CardReaderSwiftError.disconnectionError;
      case -10:
        return CardReaderSwiftError.initError;
      case -11:
        return CardReaderSwiftError.readerNotSupported;
      case -12:
        return CardReaderSwiftError.licenseFileError;
      case -13:
        return CardReaderSwiftError.parameterError;
      case -15:
        return CardReaderSwiftError.internetError;
      case -16:
        return CardReaderSwiftError.cardNotFound;
      case -18:
        return CardReaderSwiftError.licenseUpdateError;
      case -19:
        return CardReaderSwiftError.licenseUpdatedPleaseRestart;
      default:
        return CardReaderSwiftError.internalError;
    }
  }
}

extension CardReaderSwiftErrorTranslation on CardReaderSwiftError {
  String getErrorDescription(AppLocalizations locale) {
    final translation = locale;
    switch (this) {
      case CardReaderSwiftError.success:
        return translation.successNoErrorTxt;
      case CardReaderSwiftError.internalError:
        return translation.internalError;
      case CardReaderSwiftError.invalidLicense:
        return translation.invalidLicense;
      case CardReaderSwiftError.readerNotFound:
        return translation.readerNotFound;
      case CardReaderSwiftError.connectionError:
        return translation.connectionError;
      case CardReaderSwiftError.getPhotoError:
        return translation.getPhotoError;
      case CardReaderSwiftError.getTextError:
        return translation.getPersonalInfoTxtError;
      case CardReaderSwiftError.invalidCard:
        return translation.invalidCard;
      case CardReaderSwiftError.unknownCardVersion:
        return translation.unknownCardVersion;
      case CardReaderSwiftError.disconnectionError:
        return translation.disconnectionError;
      case CardReaderSwiftError.initError:
        return translation.initError;
      case CardReaderSwiftError.readerNotSupported:
        return translation.readerNotSupportedError;
      case CardReaderSwiftError.licenseFileError:
        return translation.licenseFileError;
      case CardReaderSwiftError.parameterError:
        return translation.parameterError;
      case CardReaderSwiftError.internetError:
        return translation.internetError;
      case CardReaderSwiftError.cardNotFound:
        return translation.cardNotFoundError;
      case CardReaderSwiftError.licenseUpdateError:
        return translation.licenseUpdateError;
      case CardReaderSwiftError.licenseUpdatedPleaseRestart:
        return translation.licenseUpdateSuccess;
    }
  }
}
