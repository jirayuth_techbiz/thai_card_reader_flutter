import Foundation
import RxSwift

let FLVL_OPENLIB_MODE = 1
let ACTIVE_OPENLIB_MODE = FLVL_OPENLIB_MODE
let FL_LIC_FILE = "lic/rdnidlib.dlt"

/// For [PercentEvent]
var mPercent: Int64?

class CardReaderSwiftAPIImpl : NSObject, CardReaderSwiftAPI {
    static let shared: CardReaderSwiftAPIImpl = {
        return CardReaderSwiftAPIImpl()
    }()
    fileprivate var mNiOS : NiOS!
    
    let hardwareStateIsEnable = BehaviorSubject<Bool>(value: false)
    let cardStateIsEnable = BehaviorSubject<Bool>(value: false)
    let percentageProgress = BehaviorSubject<Int64>(value: 50);
    
    let licenseUpdatedPleaseRestart: Int64 = -19

    
    private var mReaderFtInterface : ReaderInterface?
    
    var mReaderLists: NSMutableArray = []
    
    ///for object
    var mNotifyId: String = ""
    var mMessageType: String = ""
    var mCaller: String = ""
    var mArgData: NSObject?
    
    enum CardReaderInitilizeError: Error {
        case delegateSettingFailure
    }
    
    func initializeCardReader() throws -> InitializeResponse {
        
        var resResult:String = ""
        var _:Int64 = 0
        do {
            mNiOS = NiOS();
            mReaderFtInterface = ReaderInterface();
            mReaderFtInterface?.setDelegate(self)
            
            if mReaderFtInterface == nil { throw CardReaderInitilizeError.delegateSettingFailure }

            NotificationCenter.default.addObserver(self, selector: #selector(updateBLEItemView(_:)), name: NSNotification.Name(rawValue: NiOS_NotifyMessage), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(updateProgressView(_:)), name: NSNotification.Name(rawValue: NiOS_NotifyMessage), object: nil)

            hardwareStateIsEnable.onNext(false)
            cardStateIsEnable.onNext(false)
            
            resResult = "Initialized";
            return InitializeResponse(result: resResult);
        } catch let error as CardReaderInitilizeError {
            switch (error) {
                case .delegateSettingFailure:
                    return InitializeResponse(debugLog: "Failed to set delegate interface", errorCode: -1)
            }
            
        } catch {
            return InitializeResponse(debugLog: "Unknown Error", errorCode: -1)
        }
    }
    
    func getScanReaders() throws -> GetReadersResponse {
        /// BLE doesn't need license
        var resError: Int64;
        let scanReaders: Int64 = Int64(scanBleReaderThread())
        if scanReaders < 0 {
            
            resError = scanReaders
            
            return GetReadersResponse(errorCode: resError);
        }
        
        return GetReadersResponse(notifyId: mNotifyId, messageType: mMessageType, caller: mCaller,perc: mPercent,argData: mArgData);
    }
    
    func connect(request: ConnectRequest) throws -> ConnectResponse {
//        var libpath="";
//        var openModeLib : Int;
//        openModeLib = getLICPath(path: &libpath );
        
        
        var res : Int32?;
        let readerName:NSMutableString = NSMutableString(string: request.readerName!)
        
        res = mNiOS.openLibNi("") /// BLE doesn't need license
        if res != 0 {
            var error: Int64;
            var errorLog: String;
            
            error = Int64(res!);
            
            errorLog = String(format:"Error Code : %d", error);
            return ConnectResponse(result: false,debugLog:errorLog , errorCode: error)
        }
        
        res = mNiOS.selectReaderNi(readerName)
        
        let readerInfo = NSMutableString.init();
        if res != 0 {
            var error: Int64;
            var errorLog: String;
            var ntype = getReaderInfo( nsData: readerInfo );
            error = Int64(res!);
                // DONT DELETE YET MAY NEED FOR BUL OPT
//            if res == NI_INVALID_LICENSE {
//                mNiOS.closeLibNi();
//                return ConnectResponse(errorCode: error)
//            }
//            
//            if openModeLib == FLVL_OPENLIB_MODE {
//                res = mNiOS?.updateLicenseFileNi()
//                if res != 0 {
//                    errorLog = String(format: "Reader Name: %@\nReader Info: %@\nfunction updateLicenseFileNi: (error code %d)",
//                                      request.readerName!, readerInfo, error)
//                    return ConnectResponse(debugLog: errorLog, errorCode: error)
//                } else {
//                    error = licenseUpdatedPleaseRestart
//                    errorLog = String(format: "Reader Name: %@Reader Info: %@", request.readerName!, readerInfo);
//                    return ConnectResponse(debugLog: errorLog, errorCode: error)
//                }
//            }
//            
//            mNiOS?.closeLibNi()
            errorLog = String(format:"Unknown Error, Reader Info: %@\nError Code %d", readerInfo, error);
            return ConnectResponse(result: false,debugLog:errorLog , errorCode: error)
        } else {
            var msg: String = String(format: "%@  %@" ,request.readerName!,readerInfo);
            return ConnectResponse(result: true, debugLog: msg)
        }
    }

    ///Must work alongside [readCardImage]
    func readCardTextInfo(request: GetCardInfoRequest) throws -> GetCardInfoResponse {
        var res: Int32?
        let nsData: NSMutableString = NSMutableString()
        res = mNiOS?.connectCardNi()
        var errorCode = Int64(res!)
        if res != 0 {
            return GetCardInfoResponse(debugLog: "function connectCardNi Error", errorCode: errorCode)
        }
        
        
        res = mNiOS?.getNIDTextNi(nsData)
        errorCode = Int64(res!)
        if res != 0 {

            res = mNiOS?.disconnectCardNi()
            return GetCardInfoResponse(debugLog: "function getNIDTextNi Error", errorCode: errorCode)
        }
        
        return GetCardInfoResponse(notifyId: mNotifyId, messageType: mMessageType, caller: mCaller, perc: mPercent, argData: mArgData)
    }
    
    /// Must work alongside [readCardTextInfo]
    func readCardImage(request: GetCardInfoRequest) -> GetCardInfoResponse {
        var res: Int32?
        let dataPhoto = NSMutableData.init();
        res = mNiOS?.getNIDPhotoNi(dataPhoto)
        var errorCode = Int64(res!)
        if res != 0 {
            res = mNiOS?.disconnectCardNi()
            return GetCardInfoResponse(debugLog: "function getNIDPhotoNi error", errorCode: errorCode)
        }
        res = mNiOS?.disconnectCardNi()
        errorCode = Int64(res!)
        if res != 0 {

            return GetCardInfoResponse(debugLog: "function disconnectCardNi error", errorCode: errorCode)
            
        }

        return GetCardInfoResponse(notifyId: mNotifyId, messageType: mMessageType, caller: mCaller, perc: mPercent, argData: mArgData)
    }
    
    func disconnect() throws -> DisconnectResponse {
        var res: Int32?;
        
        res = mNiOS?.deselectReaderNi()
        var errorCode = Int64(res!)
        if res != 0 {
            
            return DisconnectResponse(debugLog: "function deselectReaderNi Error", errorCode: errorCode)
        }
        
        res = mNiOS?.closeLibNi()
        
        if res != 0 {
            return DisconnectResponse(debugLog: "function closeLibNi Error", errorCode: errorCode)
        }

        return DisconnectResponse(result: true)
    }
    
    private func scanBleReaderThread()  -> Int {
        var detectRenderListBLE:NSMutableArray
        
        detectRenderListBLE = NSMutableArray()
        var timeout: Int32 = 5;
        
        let resBLE: Int = Int(exactly:mNiOS.scanReaderListBleNi(detectRenderListBLE, timeout))!
        
        return resBLE
    }
    
    private func getLICPath(path:inout String ) -> Int {
        var type:Int = ACTIVE_OPENLIB_MODE
        
        switch type
        {
        case FLVL_OPENLIB_MODE:
            var libpath :[ String];
            libpath = NSSearchPathForDirectoriesInDomains( FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true);
            
            path = libpath[0]+"/"+FL_LIC_FILE;
            break;
        default:
            break
        }
        return type;
    }
    
    private func getReaderInfo(nsData: NSMutableString) -> Int32 {
        let res : Int32 = (mNiOS?.getReaderInfoNi(nsData))!
        if res != 0 {
            nsData.append("Reading...")
        }
        return res;
    }
    
    @objc private func updateBLEItemView(_ notification: Notification) {
        if let userInfo  = notification.userInfo, let info = NotificationUserInfo(userInfo: userInfo) {
            
            if info.Caller.compare("scanReaderListBleNi") == ComparisonResult.orderedSame {
                
                mNotifyId = info.NotifyID
                mMessageType = info.MessegeType
                mCaller = info.Caller
                mPercent = info.perc ?? 0
                mArgData = info.arg
                // Check if info.arg is a String
                percentageProgress.onNext(mPercent!)
                if let readerName = info.arg as? String {
                    mReaderLists.add(readerName)
                }
                // Check if info.arg is an Array of Strings
                else if let readerNames = info.arg as? [String] {
                    for readerName in readerNames {
                        mReaderLists.add(readerName)
                    }
                }
                // Handle unexpected type
                else {
                    print("Unexpected type for info.arg")
                }
            }
        }
    }
        
    @objc private func updateProgressView(_ notification: Notification) {
        if let userInfo  = notification.userInfo, let info = NotificationUserInfo(userInfo: userInfo) {
            if info.Caller.compare("getNIDTextNi") == ComparisonResult.orderedSame ||
                info.Caller.compare("getATextNi") == ComparisonResult.orderedSame {
                percentageProgress.onNext(info.perc ?? 0)
                if info.perc! >= 100 {
                    mNotifyId = info.NotifyID
                    mMessageType = info.MessegeType
                    mCaller = info.Caller
                    mPercent = info.perc ?? 0
                    mArgData = info.arg
                }
            }
            
            if info.Caller.compare("getNIDPhotoNi") == ComparisonResult.orderedSame {
                percentageProgress.onNext(info.perc ?? 0)
                if info.perc! >= 100 {
                    mNotifyId = info.NotifyID
                    mMessageType = info.MessegeType
                    mCaller = info.Caller
                    mPercent = info.perc ?? 0
                    mArgData = info.arg
                }
            }
        }
    }
    
    struct NotificationUserInfo {
        var NotifyID: String
        var MessegeType: String
        var Caller: String
        var perc: Int64?
        var arg: NSObject?
        
        init?(userInfo: [AnyHashable: Any]) {
            guard let notifyId = userInfo["NotifyId"] as? String,
                  let messegeType = userInfo["MessageType"] as? String,
                  let caller = userInfo["Caller"] as? String else {
                return nil
            }
            self.NotifyID = notifyId
            self.MessegeType = messegeType
            self.Caller = caller
            self.perc = userInfo["Percent"] as? Int64
            self.arg = userInfo["ArgData"] as? NSObject
        }
    }
    
}


extension CardReaderSwiftAPIImpl: ReaderInterfaceDelegate {
    func findPeripheralReader(_ readerName: String!) {
        
    }
    
    func readerInterfaceDidChange(_ attached: Bool, bluetoothID: String!) {
        
    }
    
    func didGetBattery(_ battery: Int) {
        
    }
    
    
    @objc func ShowHardwareStateON(_ parm: AnyObject) {
        hardwareStateIsEnable.onNext(true)
    }
    
    @objc func ShowHardwareStateOFF(_ parm: AnyObject) {
        hardwareStateIsEnable.onNext(false)
    }
    
    @objc func ShowCardStateON(_ parm: AnyObject) {
        cardStateIsEnable.onNext(true)
    }
    
    @objc func ShowCardStateOFF(_ parm: AnyObject) {
        cardStateIsEnable.onNext(false)
    }
    
    
    /**
     reader is Attach or DisAttach
     - parameter attached: void
     */
    func readerInterfaceDidChange(_ attached: Bool) {
//        //"Experiment only, don't use for real product"
        /// It's working tho
        if attached
        {
            DispatchQueue.main.async(execute: { () -> Void in
                self.performSelector(onMainThread: #selector(CardReaderSwiftAPIImpl.ShowHardwareStateON(_:)), with: Int(truncating: true), waitUntilDone: true)
            })
        } else{
            DispatchQueue.main.async(execute: { () -> Void in
                self.performSelector(onMainThread: #selector(CardReaderSwiftAPIImpl.ShowHardwareStateOFF(_:)), with: Int(truncating: true), waitUntilDone: true)
            })
        }

        CardReaderSwiftAPIImpl.shared.hardwareStateIsEnable.onNext(attached)
    }
    
    /**
     card Attach or DisAttach
     
     - parameter attached: void
     */
    func cardInterfaceDidDetach(_ attached: Bool) {
//        //"Experiment only, don't use for real product."
          /// It's working tho
        if attached
        {
            self.performSelector(onMainThread: #selector(CardReaderSwiftAPIImpl.ShowCardStateON(_:)), with: Int(truncating: true), waitUntilDone: true)
            
        } else{
            self.performSelector(onMainThread: #selector(CardReaderSwiftAPIImpl.ShowCardStateOFF(_:)), with: Int(truncating: true), waitUntilDone: true)
        }
        CardReaderSwiftAPIImpl.shared.cardStateIsEnable.onNext(attached)
    }
}
