import Cocoa
import FlutterMacOS

@NSApplicationMain
class AppDelegate: FlutterAppDelegate {
  override func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
    return true
  }
  
  override func applicationDidFinishLaunching(_ notification: Notification) {
    let controller : FlutterViewController = mainFlutterWindow?.contentViewController as! FlutterViewController
    
    NiOS.setReaderTypeNi( READERTYPE_BLE )

    let cardReaderServiceAPI = CardReaderSwiftAPIImpl()
    CardReaderSwiftAPISetup.setUp(binaryMessenger: controller.engine.binaryMessenger, api: cardReaderServiceAPI)

    let cardStatusHandlerEvent = FlutterEventChannel(name: "cardStatusHandlerEvent", binaryMessenger: controller.engine.binaryMessenger)
    cardStatusHandlerEvent.setStreamHandler(CardStatusEvent())

    let cardCurrentlyReadHandlerEvent = FlutterEventChannel(name: "cardCurrentlyReadHandlerEvent", binaryMessenger: controller.engine.binaryMessenger)
    cardCurrentlyReadHandlerEvent.setStreamHandler(CardReadingEvent())

    let percentHandlerEvent = FlutterEventChannel(name: "percentHandlerEvent", binaryMessenger: controller.engine.binaryMessenger)
    percentHandlerEvent.setStreamHandler(PercentEvent())  
  }
}
