//
//  cardStatusStreamHandler.swift
//  Runner
//
//  Created by Tcb Devbuild on 1/7/2567 BE.
//

import Foundation
import RxSwift
#if os(iOS)
  import Flutter
#elseif os(macOS)
  import FlutterMacOS
#endif

class CardStatusEvent: NSObject, FlutterStreamHandler {
    fileprivate var mIsCardConnect:Bool? = false;
    
    private var eventSink: FlutterEventSink?
    
    private let IsCardConnectStream = BehaviorSubject<Bool?>(value: false)
    
    private let disposeBag = DisposeBag()
    
    override init() {
        super.init()
    }
    
    func onListen(withArguments arguments: Any?, eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink;
        
        CardReaderSwiftAPIImpl.shared.cardStateIsEnable.subscribe(onNext: {[weak self] isConnected in
            DispatchQueue.main.async {
                let IsCardConnect = isConnected
                self!.eventSink!(IsCardConnect)
            }
        }).disposed(by:disposeBag)
        
        return nil;
        
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil;
        return nil;
    }
}

class CardReadingEvent: NSObject, FlutterStreamHandler {
    fileprivate var mIsCardDuringRead:Bool? = false;
    
    private var eventSink: FlutterEventSink?
    
    private let IsCardCurrentlyReadingStream = BehaviorSubject<Bool?>(value: false)
    
    private let disposeBag = DisposeBag()
    
    override init() {
        super.init()
    }
    
    func onListen(withArguments arguments: Any?, eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink;
        
        CardReaderSwiftAPIImpl.shared.hardwareStateIsEnable.subscribe(onNext: {[weak self] isConnected in
            DispatchQueue.main.async {
                let IsCardCurrentlyReading = isConnected
                self!.eventSink!(IsCardCurrentlyReading)
            }}).disposed(by:disposeBag)

        return nil;
        
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil;
        return nil;
    }
}
