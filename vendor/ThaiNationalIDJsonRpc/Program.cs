﻿using Nerdbank.Streams;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StreamJsonRpc;
using System.Net.Sockets;
using System.Reflection;
using ThaiNationalIDCard;

namespace ThaiNationalIDJsonRpc
{
    public class CardReaderService()
    {
        private readonly ThaiIDCard thaiIDCard = new();

        ///Request Format
        ///Content-Length: 66
        /// 
        /// {
        ///   "jsonrpc":"2.0",
        ///   "method":"Ping",
        ///   "params":["Hello worker"],
        ///   "id":1
        /// }
        /// Response Format
        /// 
        /// Content-Length: 54
        /// 
        /// {
        ///   "jsonrpc":"2.0",
        ///   "id":1,
        ///   "result":"Pong: Hello worker"
        /// }
        /// 
        public string Ping(string ping) => $"Pong: {ping}";

        public string[] GetReaders()
        {

            string[] result = thaiIDCard.GetReaders();
            return result;
        }

        public string GetVersion()
        {
            string res = thaiIDCard.Version();
            return res;
        }

        public string readCard(string reader)
        {
            //var tis620Encoding = System.Text.Encoding.GetEncoding("TIS-620");
            Personal personal = thaiIDCard.readAllPhoto(readerName: reader);

            ///// TIS620 Conversion issue + PhotoBitmap property error issue
            /// Solution : Ignore PhotoBitmap property
            var resolver = new IgnorePropertyContractResolver(new[] { "PhotoBitmap" });
            var settings = new JsonSerializerSettings
            {
                ContractResolver = resolver
            };
            var jsonString = JsonConvert.SerializeObject(personal, settings);
            //byte[] decoded = tis620Encoding.GetBytes(jsonString);
            if (thaiIDCard.ErrorCode() > 0)
            {
                ///TODO : return error
            }
            return jsonString;
        }

        public bool getReader(string reader)
        {
           var result = thaiIDCard.MonitorStart(reader);
           return result;
        }

        public bool stopReader(string reader)
        {
            var result = thaiIDCard.MonitorStop(reader);

            return result;
        }

        public bool onCardInserted(string reader)
        {
            var result = thaiIDCard.Open(reader);
            return result;
        }
    }

    public static class ThaiNationIDJsonRpcServer
    {
        static async Task Main(string[] args)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            await StartAsync(new CardReaderService());
        }

        public static async Task StartAsync<TServer>(TServer server)
        {
            await Console.Error.WriteLineAsync($"csharp-json-rpc server starting...");
            var streamSdtIo = FullDuplexStream.Splice(
                Console.OpenStandardInput(),
                Console.OpenStandardOutput()
            );
            var formatter = new JsonMessageFormatter();
            formatter.JsonSerializer.ContractResolver = new CamelCasePropertyNamesContractResolver();

            JsonRpc jsonRpc = new(new HeaderDelimitedMessageHandler(streamSdtIo, streamSdtIo, formatter));

            jsonRpc.AddLocalRpcTarget(
                server!,
                new JsonRpcTargetOptions
                {
                    DisposeOnDisconnect = true,
                });
            jsonRpc.StartListening();
            await Console.Error.WriteLineAsync($"csharp-json-rpc server started. Waiting for requests...");
            await jsonRpc.Completion;
            await Console.Error.WriteLineAsync($"csharp-json-rpc server terminated.");
        }
    }
}

public class IgnorePropertyContractResolver : DefaultContractResolver
{
    private readonly HashSet<string> _propertiesToIgnore;

    public IgnorePropertyContractResolver(IEnumerable<string> propertiesToIgnore)
    {
        _propertiesToIgnore = new HashSet<string>(propertiesToIgnore);
    }

    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
    {
        JsonProperty property = base.CreateProperty(member, memberSerialization);

        if (_propertiesToIgnore.Contains(property.PropertyName))
        {
            property.ShouldSerialize = instance => false;
        }

        return property;
    }
}